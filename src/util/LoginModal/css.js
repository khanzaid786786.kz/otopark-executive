import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  contentMainDiv: {
    outline: "none",
    border: "none"
  },
  contentDiv: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  okButton: {
    margin: "10px",
    padding: "10px",
    backgroundColor: "#00BBDC",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#00BBDC",
      border: "1px solid #00BBDC"
    }
  }
}));
