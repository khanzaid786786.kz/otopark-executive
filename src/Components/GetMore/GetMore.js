import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { useStyles } from "./css";
import ButtonAppBar from "../Appbar/Appbar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import LocalParkingIcon from "@material-ui/icons/LocalParking";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListAltIcon from "@material-ui/icons/ListAlt";
import InboxIcon from "@material-ui/icons/Inbox";
import {
  Divider,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  TextField
} from "@material-ui/core";
import Tab from "../Tab/Tab";
import { Link, Redirect } from "react-router-dom";
import DailySummary from "../DailySummary/DailySummary";
import AboutPage from "../About/About";
import Report from "../Report/Report";
import Help from "../Help/Help";
import Contact from "../Contact/Contact";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import PublicIcon from "@material-ui/icons/Public";
import CallIcon from "@material-ui/icons/Call";
import AssignmentIcon from "@material-ui/icons/Assignment";
import SettingsIcon from "@material-ui/icons/Settings";
import MoneyIcon from "@material-ui/icons/Money";
import FeedbackIcon from "@material-ui/icons/Feedback";
import ServerConfig from "../ServerConfig/ServerConfig";
import InReport from "../Report/InReport/InReport";
import OutReport from "../Report/OutReport/OutReport";

export default function GetMore() {
  const classes = useStyles();
  const [list, setList] = useState("daily");
  const [payload, setPayload] = useState(sessionStorage.getItem("payload"));

  const ListFunction = value => {
    console.log(value);
    setList(value);
  };

  const data = JSON.parse(payload);
  console.log(data);
  const isActive = value => {
    if (list === value) {
      return {
        color: "#fff",
        backgroundColor: "#00BBDC",
        paddingTop: "10px",
        paddingBottom: "10px"
      };
    } else {
      return {
        paddingTop: "10px",
        paddingBottom: "10px"
      };
    }
  };

  const isIcon = value => {
    if (list === value) {
      return {
        width: "35px",
        height: "35px",
        color: "#fff"
      };
    } else {
      return {
        width: "35px",
        height: "35px"
      };
    }
  };

  return (
    <>
      <ButtonAppBar />
      <div className={classes.root}>
        <Grid container spacing={2} className={classes.desktopView}>
          <Grid item xs={12} sm={12} md={8} lg={8}>
            <List component="nav" aria-label="main mailbox folders">
              <ListItem
                button
                style={isActive("daily")}
                onClick={() => ListFunction("daily")}
              >
                <ListItemIcon>
                  <AssignmentIcon style={isIcon("daily")} />
                </ListItemIcon>
                <ListItemText primary="Daily Summary" />
              </ListItem>
              <Divider />
              <ListItem
                button
                style={isActive("report")}
                onClick={() => ListFunction("report")}
              >
                <ListItemIcon>
                  <ListAltIcon style={isIcon("report")} />
                </ListItemIcon>
                <ListItemText primary="Report" />
              </ListItem>
              <Divider />
              <ListItem
                button
                style={isActive("inreport")}
                onClick={() => ListFunction("inreport")}
              >
                <ListItemIcon>
                  <ListAltIcon style={isIcon("inreport")} />
                </ListItemIcon>
                <ListItemText primary="In Report" />
              </ListItem>
              <Divider />
              <ListItem
                button
                style={isActive("outreport")}
                onClick={() => ListFunction("outreport")}
              >
                <ListItemIcon>
                  <ListAltIcon style={isIcon("outreport")} />
                </ListItemIcon>
                <ListItemText primary="Out Report" />
              </ListItem>
              <Divider />
              {/*}
                  <ListItem
                    button
                    style={isActive("help")}
                    onClick={() => ListFunction("help")}
                  >
                    <ListItemIcon>
                      <HelpOutlineIcon style={isIcon("help")} />
                    </ListItemIcon>
                    <ListItemText primary="Help" />
                  </ListItem>
                  <Divider /> */}
              <ListItem
                button
                style={isActive("contact")}
                onClick={() => ListFunction("contact")}
              >
                <ListItemIcon>
                  <CallIcon style={isIcon("contact")} />
                </ListItemIcon>
                <ListItemText primary="Contact Agent" />
              </ListItem>
              <Divider />
              <a
                href="https://www.otopark.in/faqs/"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem
                  button
                  style={isActive("faq")}
                  onClick={() => ListFunction("faq")}
                >
                  <ListItemIcon>
                    <FeedbackIcon style={isIcon("faq")} />
                  </ListItemIcon>
                  <ListItemText primary="F.A.Q" />
                </ListItem>
              </a>
              <Divider />
              <ListItem
                button
                style={isActive("about")}
                onClick={() => ListFunction("about")}
              >
                <ListItemIcon>
                  <LocalParkingIcon style={isIcon("about")} />
                </ListItemIcon>
                <ListItemText primary="About" />
              </ListItem>
              <Divider />
              <a
                href="https://www.otopark.in/"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem
                  button
                  style={isActive("website")}
                  onClick={() => ListFunction("website")}
                >
                  <ListItemIcon>
                    <PublicIcon style={isIcon("website")} />
                  </ListItemIcon>
                  <ListItemText primary="Website" />
                </ListItem>
              </a>
              <Divider />
              {/* <ListItem
                    button
                    style={isActive("payment")}
                    onClick={() => ListFunction("payment")}
                  >
                    <ListItemIcon>
                      <MoneyIcon style={isIcon("payment")} />
                    </ListItemIcon>
                    <ListItemText primary="Payment" />
                  </ListItem> */}
              {/* <Divider />
                  <ListItem
                    button
                    style={isActive("config")}
                    onClick={() => ListFunction("config")}
                  >
                    <ListItemIcon>
                      <SettingsIcon style={isIcon("config")} />
                    </ListItemIcon>
                    <ListItemText primary="Configuration" />
                  </ListItem> */}
            </List>
          </Grid>
          <Grid item xs={12} sm={12} md={4} lg={4} className={classes.gridFour}>
            <div>
              {list === "daily" ? (
                <DailySummary />
              ) : list === "about" ? (
                <AboutPage />
              ) : list === "report" ? (
                <Report />
              ) : list === "inreport" ? (
                <InReport />
              ) : list === "outreport" ? (
                <OutReport />
              ) : list === "help" ? (
                <Help />
              ) : list === "contact" ? (
                <Contact />
              ) : list === "config" ? (
                <ServerConfig />
              ) : (
                ""
              )}
            </div>
          </Grid>
        </Grid>
        <Tab />
      </div>

      {/* MOBile View */}
      <div>
        <Grid
          container
          spacing={2}
          className={classes.mobileView}
          style={{ padding: "20px", marginBottom: "80px" }}
        >
          <Grid item xs={12} sm={12} md={8} lg={8}>
            <div>
              <Avatar
                style={{
                  width: "100px",
                  height: "100px",
                  marginTop: "10px",
                  marginLeft: "30px"
                }}
                alt="Remy Sharp"
                src={data.avator}
              />
              <div style={{ marginLeft: "150px", marginTop: "-110px" }}>
                <h3>{data.first_name + " " + data.last_name}</h3>
                <p style={{ marginTop: "-10px" }}>
                  {data.parking_address}
                  <br />
                  <div> Ranger ID: {data.ranger_id}</div>
                </p>
              </div>
            </div>

            <List component="nav" aria-label="main mailbox folders">
              <Link
                to="/otopark/executive/summary"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem
                  button
                  style={{ padding: "10px" }}
                  onClick={() => ListFunction("daily")}
                >
                  <ListItemIcon>
                    <AssignmentIcon style={{ fontSize: "35px" }} />
                  </ListItemIcon>
                  <ListItemText primary="Daily Summary" />
                </ListItem>
              </Link>
              <Divider />
              <Link
                to="/otopark/executive/report"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem button style={{ padding: "10px" }}>
                  <ListItemIcon>
                    <ListAltIcon style={isIcon("report")} />
                  </ListItemIcon>
                  <ListItemText primary="Report" />
                </ListItem>
              </Link>
              <Divider />
              <Link
                to="/otopark/executive/help"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem
                  button
                  style={{ padding: "10px" }}
                  onClick={() => ListFunction("help")}
                >
                  <ListItemIcon>
                    <HelpOutlineIcon style={isIcon("help")} />
                  </ListItemIcon>
                  <ListItemText primary="Help" />
                </ListItem>
              </Link>
              <Divider />
              <Link
                to="/otopark/executive/contact"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem
                  button
                  style={{ padding: "10px" }}
                  onClick={() => ListFunction("contact")}
                >
                  <ListItemIcon>
                    <CallIcon style={isIcon("contact")} />
                  </ListItemIcon>
                  <ListItemText primary="Contact Agent" />
                </ListItem>
              </Link>
              <Divider />
              <a
                href="https://www.otopark.in/faqs/"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem
                  button
                  style={{ padding: "10px" }}
                  onClick={() => ListFunction("faq")}
                >
                  <ListItemIcon>
                    <FeedbackIcon style={isIcon("faq")} />
                  </ListItemIcon>
                  <ListItemText primary="F.A.Q" />
                </ListItem>
              </a>
              <Divider />
              <Link
                to="/otopark/executive/about"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem
                  button
                  style={{ padding: "10px" }}
                  onClick={() => ListFunction("about")}
                >
                  <ListItemIcon>
                    <LocalParkingIcon style={isIcon("about")} />
                  </ListItemIcon>
                  <ListItemText primary="About" />
                </ListItem>
              </Link>
              <Divider />
              <a
                href="https://www.otopark.in/"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem
                  button
                  style={{ padding: "10px" }}
                  onClick={() => ListFunction("website")}
                >
                  <ListItemIcon>
                    <PublicIcon style={isIcon("website")} />
                  </ListItemIcon>
                  <ListItemText primary="Website" />
                </ListItem>
              </a>
              <Divider />
              <Link
                to="/otopark/executive/config"
                style={{ textDecoration: "None", color: "black" }}
              >
                <ListItem button style={{ padding: "10px" }}>
                  <ListItemIcon>
                    <SettingsIcon style={isIcon("config")} />
                  </ListItemIcon>
                  <ListItemText primary="Configuration" />
                </ListItem>
              </Link>
            </List>

            <Button
              justify="center"
              style={{
                width: "80%",
                marginTop: "70px",
                marginLeft: "35px",
                backgroundColor: "#00BBDC",
                color: "#fff",
                borderRadius: "20px",
                fontSize: "17px"
              }}
            >
              Logout
            </Button>
          </Grid>
          <Tab />
        </Grid>
      </div>
    </>
  );
}
