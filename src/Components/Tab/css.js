import { makeStyles, fade } from "@material-ui/core/styles";
import { borderColor } from "@material-ui/system";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  mobileTab: {
    marginLeft: "-15px",
    width: "66%",
    position: "fixed",
    bottom: "0%",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "-5px",
      width: "100%",
      position: "fixed",
      bottom: "0%",
      zIndex: "9999"
    }
  },
  tabButton: {
    width: "33%"
  },
  tabText: {
    "&:active": {
      color: "white"
    }
  }
}));
