import React, { Suspense, lazy } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";

import Loader from "../util/Loader/Loader";

const LoginPage = lazy(() => import("../Components/LoginPage/LoginPage"));

const GetIn = lazy(() => import("../Components/GetIn/GetIn"));
const GetOut = lazy(() => import("../Components/GetOut/GetOut"));
const GetMore = lazy(() => import("../Components/GetMore/GetMore"));
const About = lazy(() => import("../Components/About/About"));
const Help = lazy(() => import("../Components/Help/Help"));
const Contact = lazy(() => import("../Components/Contact/Contact"));
const DailySummary = lazy(() =>
  import("../Components/DailySummary/DailySummary")
);
const ServerConfig = lazy(() =>
  import("../Components/ServerConfig/ServerConfig")
);
const Report = lazy(() => import("../Components/Report/Report"));

const NotFound = lazy(() => import("../util/NotFound/NotFound"));

export const Routes = () => {
  return (
    <HashRouter>
      <Suspense fallback={<Loader />}>
        <Switch>
          <Route exact path="/otopark/executive/login" component={LoginPage} />

          <Route exact path="/otopark/executive/getin" component={GetIn} />
          <Route exact path="/otopark/executive/getout" component={GetOut} />
          <Route exact path="/otopark/executive/getmore" component={GetMore} />
          <Route exact path="/otopark/executive/about" component={About} />
          <Route exact path="/otopark/executive/help" component={Help} />
          <Route exact path="/otopark/executive/contact" component={Contact} />
          <Route
            exact
            path="/otopark/executive/summary"
            component={DailySummary}
          />
          <Route
            exact
            path="/otopark/executive/config"
            component={ServerConfig}
          />
          <Route exact path="/otopark/executive/report" component={Report} />

          <Route exact path="*" component={NotFound} />
        </Switch>
      </Suspense>
    </HashRouter>
  );
};
