import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Grid container spacing={3}>
            <Grid xs={12} sm={12} md={2} lg={2}></Grid>
            <Grid item xs={12} sm={12} md={8} lg={8}>
              <div className={classes.desktopView}>
              <Link to="/otopark/executive/getmore">
                <ArrowBackIcon style={{ color: "#00BBDC" }} />
                </Link>
              </div>

              <img
                className={classes.loginImage}
                src={require("../../assets/images/LoginPageImg.png")}
              />
              <br />
              <form align="center">
                <label className={classes.label}>Issue Name</label> <br />
                <TextField
                  style={{ backgroundColor: "#F7F7F7" }}
                  InputProps={{
                    classes: {
                      input: classes.resize
                    }
                  }}
                  fullWidth
                  size="small"
                  variant="outlined"
                  name="title"
                />
                <br />
                <br />
                <br />
                <label className={classes.label}>Description</label> <br />
                <TextField
                  style={{ backgroundColor: "#F7F7F7" }}
                  InputProps={{
                    classes: {
                      input: classes.resize
                    }
                  }}
                  fullWidth
                  multiline
                  rows={5}
                  size="small"
                  variant="outlined"
                  name="title"
                />
                <br />
                <br />
                <Button className={classes.submitButton}>Submit</Button>
              </form>
            </Grid>
            <Grid xs={12} sm={12} md={2} lg={2}></Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
