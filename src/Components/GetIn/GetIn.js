import React, { useState, useEffect, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { useStyles } from "./css";
import Appbar from "../Appbar/Appbar";
import ButtonAppBar from "../Appbar/Appbar";
import Tab from "../Tab/Tab";
import { ENDPOINTS } from "../../Service";

import LoginModal from "../../util/LoginModal/LoginModal";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import { Badge, Checkbox, FormControl, FormControlLabel, InputLabel, MenuItem, Select, withStyles } from "@material-ui/core";
import { BridgeCardModalCom, Modal } from "../../Modal/Modal";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import DateFnsUtils from '@date-io/date-fns';

import "react-datepicker/dist/react-datepicker.css";
import { setDate } from "date-fns";
var moment = require("moment");

export default function GetIn() {
  const classes = useStyles();
  const [select, setSelect] = useState(false);
  const [socketConnection, setSocketConnection] = useState(false);
  const [error, setError] = useState(false);
  const [errorDetails, setErrorDetails] = useState(null);
  const [errorTitle, setErrorTitle] = useState(null);
  const [vehicleType, setVehicleType] = useState(null);
  const [chooseVehicleType, setChooseVehicleType] = useState(null);
  const [VRNumber, setVRNumber] = useState("");
  const [ownerName, setOwnerName] = useState("");
  const [contactNo, setContactNo] = useState("");
  const [entryTime, setEntryTime] = useState(null);
  const [hourOrDailyActive, setHourOrDailyActive] = useState(false);
  const [weeklyPrice, setWeeklyPrice] = useState(null);
  const [monthlyPrice, setMonthlyPrice] = useState(null);
  const [selectedPriceButton, setSelectedPriceButton] = useState(null);
  const [selectedPrice, setSelectedPrice] = useState("");
  const [open, setOpen] = useState(false);
  const [paymentModal, setPaymentModal] = useState(false);
  const [startDate, setStartDate] = useState(null);
  const ws = useRef(null);
  const [feesType, setFeesType] = useState(null);
  const [anprData, setAnprData] = useState(null);
  const [anprImage, setAnprImage] = useState(null);
  const [fromRange, setFromRange] = useState(null);
  const [toRange, setToRange] = useState(null);
  const [noOfMonth, setNoOfMonth] = useState(1);
  const [total, setTotal] = useState(0)
  const [advance, setAdvance] = useState(0)
  const [discount, setDiscount] = useState(0);
  const [entryStatus, setEntryStatus] = useState(true);
  const [tagId, setTagId] = useState("");
  const [tagMessage, setTagMessage] = useState(null);
  const [organisation, setOrganisation] = useState(null);
  const [selectedOrganisation, setSelectedOrganisation] = useState(0);
  const [dailyPrice, setDailyPrice] = useState(null);
  const [hourlyPrice, setHourlyPrice] = useState(null);
  const [selectedCls, setSelectedCls] = useState(null);
  const [orgPassStatus, setOrgPassStatus] = useState(false);
  const [cardPaymentModal,setCardPaymentModal]=useState(false);
  const [cardData,setCardData]=useState(null);

  function handleCloseCardPaymentModal(){
    setCardPaymentModal(false);
  }
  function handleCardData(data){
  setCardData(data);
  }

  function handleTag(e) {
    setTagId(e.target.value.toUpperCase());
  }

  function handleOrgPassStatusChange(e) {
    setOrgPassStatus(e.target.checked);
  }
  useEffect(() => {
    ws.current = new W3CWebSocket(
      ENDPOINTS.BASE_WS_URL + ENDPOINTS.GET_IN_STREAM
    );
    console.log("web socket connected!!");
  }, []);

  useEffect(() => {
    ws.current.onopen = () => {
      console.log("WebSocket Client Connected");
      setSocketConnection(true);
      let payload = JSON.parse(sessionStorage.getItem("payload"));
      ws.current.send(
        JSON.stringify({
          access_token: payload.access_token,
          host: payload.host,
          parking_table_id: payload.parking_table_id,
          ranger_id: payload.ranger_id,
          type: "get_vehicle_types"
        })
      );
      ws.current.send(
        JSON.stringify({
          access_token: payload.access_token,
          host: payload.host,
          parking_table_id: payload.parking_table_id,
          ranger_id: payload.ranger_id,
          type: "get_latest_anpr_entry"
        })
      );
    };

    ws.current.onmessage = message => {
      console.log(message);
      let response = JSON.parse(message.data);
      if (response.status === "error") {
        setPaymentModal(false);
        setError(true);
        setErrorDetails(response.message);
        setErrorTitle("");
        setVRNumber("");
        setOwnerName("");
        setContactNo("");
        setChooseVehicleType(null);
        setSelectedPriceButton(null);
        setWeeklyPrice(null);
        setMonthlyPrice(null);
        setHourOrDailyActive(false);
        setStartDate(null);
        setTotal(0);
        setDiscount(0);
        setTotal(0);
        setAdvance(0);

        setEntryStatus(true);
        setSelectedPrice(null);
        setSelectedCls(null);
        setHourlyPrice(null);
        setDailyPrice(null);
        setSelectedOrganisation(0);
        setTagId("");


      } else {
        if (response.message === "vehicle_types") {
          if (response.organisation.length != 0) {
            setOrganisation(response.organisation)

          }
          setVehicleType(response.payload);
        } else if (response.message === "fees_types") {
          let feesTypeResp = response.payload;
          console.log(feesTypeResp);
          feesTypeResp.rates.forEach(element => {
            if (
              element.feesType === "Hourly Rates") {
              setHourOrDailyActive(true);
              if (element.active) {
                setHourlyPrice(element.fees);
              }

            }
            else if (element.feesType === "Daily Rates") {
              if (element.active) {
                setHourOrDailyActive(true);
                setDailyPrice(element.fees);
              }
            } else if (element.feesType === "Weekly Rates") {
              if (element.active) {
                setWeeklyPrice(element.fees);
              }
            } else {
              if (element.active) {
                setMonthlyPrice(element.fees);
              }
            }
          });

          setEntryStatus(true);
          setTotal(0);
          setAdvance(0);
          setDiscount(0);
        } else if (response.message === "getIn") {
          let payload = JSON.parse(sessionStorage.getItem("payload"));
          console.log(response.payload);
          setError(true);
          setErrorDetails("Success !!");
          setErrorTitle("Success");
          setVRNumber("");
          setOwnerName("");
          setContactNo("");
          setChooseVehicleType(null);
          setSelectedPriceButton(null);
          setWeeklyPrice(null);
          setMonthlyPrice(null);
          setHourOrDailyActive(false);
          setStartDate(null);
          setPaymentModal(false);

          setEntryStatus(true);
          setDiscount(0);
          setTotal(0);
          setAdvance(0);
          setSelectedPrice(null);
          setSelectedCls(null);
          setHourlyPrice(null);
          setDailyPrice(null);
          setSelectedOrganisation(0);
          setTagId("");



          try {
            console.log(response.payload);
            window.Android.printReceipt(
              response.payload.VRNumber,
              response.payload.entryTime,
              null,
              payload.parking_address,
              payload.parking_name,
              response.payload.vehicleType,
              payload.host,
              payload.parking_table_id,
              payload.ranger_id,
              response.payload.total,
              response.payload.feesType
            );
          } catch (error) {
            console.log(error);
          }
          ws.current.send(
            JSON.stringify({
              access_token: payload.access_token,
              host: payload.host,
              parking_table_id: payload.parking_table_id,
              ranger_id: payload.ranger_id,
              type: "openEntry"
            })
          );


          ws.current.send(
            JSON.stringify({
              access_token: payload.access_token,
              host: payload.host,
              parking_table_id: payload.parking_table_id,
              ranger_id: payload.ranger_id,
              type: "get_vehicle_types"
            })
          );
        } else if (response.message === "payNow") {



          let payload = JSON.parse(sessionStorage.getItem("payload"));
          setPaymentModal(false);
          setError(true);
          setErrorDetails("Success !!");
          setErrorTitle("Info");
          setVRNumber("");
          setOwnerName("");
          setContactNo("");

          setChooseVehicleType(null);
          setSelectedPriceButton(null);
          setWeeklyPrice(null);
          setMonthlyPrice(null);
          setHourOrDailyActive(false);
          setStartDate(null);
          setPaymentModal(false);
          setTotal(0);
          setDiscount(0);
          setTotal(0);
          setAdvance(0);
          setEntryStatus(true);
          setSelectedPrice(null);
          setSelectedCls(null);
          setHourlyPrice(null);
          setDailyPrice(null);
          setSelectedOrganisation(0);
          setTagId("");



          try {
            console.log(response.payload);

            window.Android.printReceipt(
              response.payload.VRNumber,
              response.payload.entryTime,
              null,
              payload.parking_address,
              payload.parking_name,
              response.payload.vehicleType,
              payload.host,
              payload.parking_table_id,
              payload.ranger_id,
              response.payload.total,
              "Pass"
            );




          } catch (error) {
            console.log(error);
          }


          ws.current.send(
            JSON.stringify({
              access_token: payload.access_token,
              host: payload.host,
              parking_table_id: payload.parking_table_id,
              ranger_id: payload.ranger_id,
              type: "openEntry"
            })
          );


          ws.current.send(
            JSON.stringify({
              access_token: payload.access_token,
              host: payload.host,
              parking_table_id: payload.parking_table_id,
              ranger_id: payload.ranger_id,
              type: "get_vehicle_types"
            })
          );
        } else if (response.message === "anpr") {

          if (response.direction === "Entry") {
            setVRNumber(response.VRNumber);
            setAnprData(response.message);
            setAnprImage(response.cropImageOfNumberPlate);
          }
        }else if(response.message==="rfid"){
          if(response.direction==="Entry"){
              setTagId(response.tagId);
              setTagMessage(response.tagMessage);
              
              if(response.VRNumber){
                setVRNumber(response.VRNumber);
              }
              
          }
        }else if(response.message==="rfidEntry"){
          if(response.direction==="Entry"){
            setTagId(response.tagId);
              setTagMessage(response.tagMessage);
              if(response.VRNumber){
                setVRNumber(response.VRNumber);
              }
            let payload = JSON.parse(sessionStorage.getItem("payload"));
        
            JSON.stringify({
              access_token: payload.access_token,
              host: payload.host,
              parking_table_id: payload.parking_table_id,
              ranger_id: payload.ranger_id,
              type: "openEntry"
            })
          }
        }else if(response.message==="rfidError"){
          if(response.direction==="Entry"){
           setTagMessage(response.tagMessage);
           setTagId(response.tagId);
           
          }
        }


      }
    };
  }, []);




  function handleOrganisationChange(e) {
    setSelectedOrganisation(e.target.value);
  }
  function handleEntryStatus(e) {
    setEntryStatus(e.target.checked);
  }
  function convertDateToFormat(date) {
    var today = date;
    var day = today.getDate() + "";
    var month = today.getMonth() + 1 + "";
    var year = today.getFullYear() + "";
    var hour = today.getHours() + "";
    var minutes = today.getMinutes() + "";
    var seconds = today.getSeconds() + "";

    day = checkZero(day);
    month = checkZero(month);
    year = checkZero(year);
    hour = checkZero(hour);
    minutes = checkZero(minutes);
    seconds = checkZero(seconds);

    return day + "-" + month + "-" + year + " " + hour + ":" + minutes;

    function checkZero(data) {
      if (data.length == 1) {
        data = "0" + data;
      }
      return data;
    }
  }
  function handleDate(date) {
    console.log(moment(date).format("DD-MM-YYYY HH:mm"));
    // console.log(date);

    setStartDate(date);
    setEntryTime(moment(date).format("DD-MM-YYYY HH:mm"));
  }

  function handleNoOfMonth(value) {
    console.log(value.target.value);
    setNoOfMonth(value.target.value);
    setTotal((selectedPrice * value.target.value) - discount);
  }

  function handleVehicleType(vehicleType) {
    setChooseVehicleType(vehicleType);

    let payload = JSON.parse(sessionStorage.getItem("payload"));
    ws.current.send(
      JSON.stringify({
        access_token: payload.access_token,
        host: payload.host,
        parking_table_id: payload.parking_table_id,
        ranger_id: payload.ranger_id,
        type: "get_fees_types",
        vehicleType: vehicleType
      })
    );
    setSelectedPriceButton(null);
    setSelectedOrganisation(0);
  }

  function proceedPay(type) {
    if (VRNumber === "") {
      setError(true);
      setErrorDetails("Enter Vehicle No !!");
      setErrorTitle("Error !!");
      return;
    }
    if (!chooseVehicleType) {
      setError(true);
      setErrorDetails("Select Vehicle Type !!");
      setErrorTitle("Error !!");
      return;
    }
    if (type === "GETIN") {
      let payload = JSON.parse(sessionStorage.getItem("payload"));
      ws.current.send(
        JSON.stringify({
          access_token: payload.access_token,
          host: payload.host,
          parking_table_id: payload.parking_table_id,
          ranger_id: payload.ranger_id,
          type: "getIn",
          VRNumber: VRNumber,
          ownerName: ownerName ? ownerName : "",
          contactNo: contactNo ? contactNo : "",
          vehicleType: chooseVehicleType,
          entryTime: entryTime ? entryTime : "",
          advance: advance,
          totalBill: total,
          organisation: selectedOrganisation ? selectedOrganisation : 0,
          orgMakePassStatus: orgPassStatus ? "Yes" : "No",
          anprImage: anprImage ? anprImage : 0,
          tagId: tagId ? tagId : 0

        })
      );
    } else {
      setPaymentModal(true);
    }
  }

  function onHandlePrice(e) {
    setSelectedPrice(e.target.value);
  }

  function proceedPayment(paymentType) {
    if (paymentType === "CASH") {
      if (advance > 0) {
        let payload = JSON.parse(sessionStorage.getItem("payload"));
        ws.current.send(
          JSON.stringify({
            access_token: payload.access_token,
            host: payload.host,
            parking_table_id: payload.parking_table_id,
            ranger_id: payload.ranger_id,
            type: "getIn",
            VRNumber: VRNumber,
            ownerName: ownerName ? ownerName : "",
            contactNo: contactNo ? contactNo : "",
            vehicleType: chooseVehicleType,
            entryTime: entryTime ? entryTime : "",
            advance: advance,
            totalBill: total,
            paymentType: paymentType,
            organisation: selectedOrganisation ? selectedOrganisation : 0,
            orgMakePassStatus: orgPassStatus ? "Yes" : "No",

            anprImage: anprImage ? anprImage : 0,
            tagId: tagId ? tagId : 0
          })
        );


      } else {

        let payload = JSON.parse(sessionStorage.getItem("payload"));
        ws.current.send(
          JSON.stringify({
            access_token: payload.access_token,
            host: payload.host,
            parking_table_id: payload.parking_table_id,
            ranger_id: payload.ranger_id,
            type: "generate_pass",
            VRNumber: VRNumber,
            ownerName: ownerName ? ownerName : "",
            contactNo: contactNo ? contactNo : "",
            vehicleType: chooseVehicleType,
            entryTime: entryTime ? entryTime : "",
            paymentType: paymentType,
            feesType: feesType,
            feesValue: selectedPrice,
            from_range: fromRange,
            to_range: toRange,
            noOfMonth: noOfMonth,
            discount: discount,
            totalBill: total,
            entryStatus: entryStatus ? "Yes" : "No",
            organisation: selectedOrganisation ? selectedOrganisation : 0,
            orgMakePassStatus: orgPassStatus ? "Yes" : "No",
            anprImage: anprImage ? anprImage : 0,
            tagId: tagId ? tagId : 0


          })
        );
      }
    }
    else {
      
      setCardPaymentModal(true);
    }

  }

  function handleInput(name, value) {
    if (name === "VRNumber") {
      setVRNumber(value.toUpperCase().replace(" ", ""));
    } else if (name === "name") {
      setOwnerName(value.toUpperCase());
    } else if (name === "contactNo") {
      setContactNo(value.toUpperCase());
    }
    // console.log(name,value);
  }

  function handleAdvance(e) {
    if (e.target.value === "") {
      setAdvance(0);
      setTotal(0);
      return;
    }
    let val = parseFloat(e.target.value);
    if (val <= 0) {
      setAdvance(0);
      setTotal(0);
    } else {
      setAdvance(val);
      setTotal(val);
    }
  }

  function handleDiscount(e) {
    let disc = parseFloat(e.target.value);

    if ((noOfMonth * selectedPrice) - disc < 0) {
      return;
    }
    if (e.target.value === "") {
      setDiscount(0);
      setTotal(noOfMonth * selectedPrice);
      return;
    }
    if (disc <= 0) {
      setDiscount(0);
      setTotal(noOfMonth * selectedPrice);

      return;
    } else {
      setDiscount(disc);
      setTotal((noOfMonth * selectedPrice) - disc);
    }
  }

  function handleHourlyPrice(price, priceBtn, feesType, from_range, to_range) {
    setSelectedPriceButton(null);

    setAdvance(price);
    setTotal(price);
    setSelectedCls(priceBtn);
  }
  function handleDailyPrice(price, priceBtn, feesType, from_range, to_range) {
    setSelectedPriceButton(null);

    setAdvance(price);
    setTotal(price);
    setSelectedCls(priceBtn);

  }
  function handlePrice(price, priceBtn, feesType, from_range, to_range) {
    console.log(feesType)
    setSelectedPrice(price);
    setSelectedPriceButton(priceBtn);
    setFeesType(feesType);
    setFromRange(from_range)
    setToRange(to_range)
    setDiscount(0);
    setNoOfMonth(1);
    setTotal(price);

  }
  function handleOpenModal() {
    setError(true);
  }
  function handleCloseModal() {
    setError(false);
  }
  function handleClose() {
    setPaymentModal(false);
  }

  const ExampleCustomInput = ({ value, onClick }) => (
    <button
      className="example-custom-input btn-block"
      onClick={onClick}
      style={{
        width: "100%",
        marginRight: "5px",
        fontSize: "20px",
        zIndex: "9999",
        backgroundColor: "#fff",
        fontWeight: "bold",
        color: "black",
        paddingTop: '5px',
        borderBottom: "2px solid white",
        borderLeft: "2px solid white",
        borderRight: "2px solid white",
        borderTop: "2px solid white"
      }}
    >
      <div>
        <img src={require('../../assets/images/calendar.png')} style={{ paddingTop: '5px', paddingRight: '5px', width: '30px', height: '30px' }} />
        {/* {!value ? "Select Date" : value} */}
      </div>
      <div>
        {value}
      </div>
    </button>
  );
  console.log(startDate);
  return (
    <>
      {cardPaymentModal && (
        <BridgeCardModalCom
          handleClose={handleCloseCardPaymentModal}
          handleCardData={handleCardData}
          open={cardPaymentModal}
          vehicleNo={VRNumber}
          amount = {total}
        />
      )}
      {paymentModal && (
        <Modal
          handleClose={handleClose}
          proceedPayment={proceedPayment}
          paymentModal={paymentModal}
        />
      )}
      {error && (
        <LoginModal
          isOpen={error}
          openModal={handleOpenModal}
          closeModal={handleCloseModal}
          message={errorDetails}
          title={errorTitle}
        />
      )}
      <ButtonAppBar />
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item lg={12}>
            <Grid container spacing={3}>
              <Grid style={{marginTop:"30px"}} item xs={12} sm={12} md={8} lg={8}>
                {/* <div className={classes.containerDiv}> */}
                <h1>Get In</h1>
                <h3 style={{ color: "#00BBDC" }}>
                  Select a Vehicle Type<span style={{ color: "red" }}>*</span>
                </h3>
                {/* vehicle grid start */}
                <Grid container spacing={3}>
                  {vehicleType &&
                    vehicleType.map(obj => (
                      <Grid item lg={2}>
                        <div
                          style={{
                            marginTop: "17px"
                          }}
                        >
                          <Badge
                            badgeContent={obj.availableSpace}
                            max={99999}
                            color="secondary"
                          >
                            <div
                              // style={{marginTop:"10px"}}
                              onClick={() =>
                                handleVehicleType(obj.Vehicle_name)
                              }
                              className={
                                chooseVehicleType &&
                                  chooseVehicleType === obj.Vehicle_name
                                  ? classes.choosenVehicle
                                  : classes.unchoosenVehicle
                              }
                            >
                              <img
                                style={{
                                  marginTop: "10px"
                                }}
                                src={obj.vehicle_image}
                                width="40px"
                                height="40px"
                              />
                              <h5>{obj.Vehicle_name}</h5>
                            </div>
                          </Badge>
                        </div>
                      </Grid>
                    ))}
                </Grid>
                <br />
                <br />
                <div className={classes.horizontalLine}></div>
                {/* <div className={classes.mobileView}> */}
                <Grid container spacing={3}>
                  <Grid item xs={12} sm={12} md={6} lg={6}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>

                          <label>
                            <b style={{ fontWeight: "600" }}>
                              Tag Id
                            </b>
                          </label>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>

                          <TextField
                          fullWidth
                            className={classes.vehicleInput}
                            InputProps={{
                              classes: {
                                input: classes.resize
                              }
                            }}
                            onChange={handleTag}

                            size="small"
                            name="tagId"
                            variant="outlined"
                            value={tagId}


                          />
                        </Grid>

                      </Grid>
                      <Grid item xs={12} sm={12} md={6} lg={6}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>

                          <label>
                            <b style={{ fontWeight: "600" }}>
                              Fastag Transaction Message
                            <span style={{ color: "red" }}>*</span>
                            </b>
                          </label>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>

                          <h3>{tagMessage ? tagMessage : "No Tag"}</h3>

                        </Grid>

                      </Grid>

                  <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <label>
                        <b style={{ fontWeight: "600" }}>
                          Vehicle Number
                            <span style={{ color: "red" }}>*</span>
                        </b>
                      </label>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <TextField
                        className={classes.vehicleInput}
                        InputProps={{
                          classes: {
                            input: classes.resize
                          }
                        }}

                        size="small"
                        name="VRNumber"
                        variant="outlined"
                        value={VRNumber}
                        onChange={e =>
                          handleInput(e.target.name, e.target.value)
                        }

                      />
                    </Grid>

                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={12}
                    md={6}
                    lg={6}
                  // className={classes.vehicleInputOwnerNumber}
                  >
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <b style={{ fontWeight: "600" }}>Number Plate Image</b>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <img
                        style={{ height: "50px", width: "150px" }}
                        src={anprImage && anprImage}
                        alt="Img Tag"
                      />
                    </Grid>
                  </Grid>

                  <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <b style={{ fontWeight: "600" }}>In Date(optional)</b>{" "}
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          format="dd/MM/yyyy"
                          value={startDate}
                          onChange={date => handleDate(date)}
                          margin="normal"
                          autoOk={true}
                          disableFuture={true}
                          emptyLabel="Select Date"
                          // closeOnScroll={true}
                          // showTimeSelect
                          // locale="sv-sv"
                          // timeFormat="HH:mm"
                          // customInput={<ExampleCustomInput />}
                          // withPortal
                          // popperModifiers={{
                          //   preventOverflow: {
                          //     enabled: true
                          //   }
                          // }}
                          inputVariant="outlined"
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                      </MuiPickersUtilsProvider>

                    </Grid>

                  </Grid>
                  <Grid item xs={12} sm={12} md={6} lg={6}>

                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <b style={{ fontWeight: "600" }}>In Time(optional)</b>{" "}
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardTimePicker
                          value={startDate}
                          onChange={date => handleDate(date)}
                          dateFormat="dd/MM/yyyy HH:mm"
                          margin="normal"
                          emptyLabel="Select Time"
                          inputVariant="outlined"

                          KeyboardButtonProps={{
                            'aria-label': 'change time',
                          }}
                        // closeOnScroll={true}
                        // showTimeSelect
                        // locale="sv-sv"
                        // timeFormat="HH:mm"
                        // customInput={<ExampleCustomInput />}
                        // withPortal
                        // popperModifiers={{
                        //   preventOverflow: {
                        //     enabled: true
                        //   }
                        // }}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                  </Grid>

                  <Grid item xs={12} sm={12} md={6} lg={6}>

                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <b style={{ fontWeight: "600" }}>
                        Vehicle Owner Number
                        </b>{" "}
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <TextField
                        className={classes.vehicleInput}
                        InputProps={{
                          classes: {
                            input: classes.resize
                          }
                        }}
                        size="small"
                        variant="outlined"
                        type="number"
                        name="contactNo"
                        value={contactNo}
                        onChange={e =>
                          handleInput(e.target.name, e.target.value)
                        }
                      />
                    </Grid>
                  </Grid>
                  <Grid item xs={12} sm={12} md={6} lg={6}>

                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <b style={{ fontWeight: "600" }}>
                        Vehicle Owner Name
                        </b>{" "}
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>


                      <TextField
                        className={classes.vehicleInput}
                        InputProps={{
                          classes: {
                            input: classes.resize
                          }
                        }}
                        size="small"
                        variant="outlined"
                        name="name"
                        value={ownerName}
                        onChange={e =>
                          handleInput(e.target.name, e.target.value)
                        }
                      />

                    </Grid>
                  </Grid>
                </Grid>
                {/* </div> */}
                {/* </div> */}
                {/* <div className={classes.verticalLine}></div> */}
              </Grid>

              <Grid item xs={12} sm={12} md={4} lg={4}>

                <br />
                <br />

                {
                  monthlyPrice ?
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <h2 style={{ color: "#00BBDC" }}>Available Passes</h2>

                    </Grid> : weeklyPrice ?
                      <Grid item xs={12} sm={12} md={12} lg={12}>
                        <h2 style={{ color: "#00BBDC" }}>Available Passes</h2>

                      </Grid> : ""

                }


                <Grid item xs={12} sm={12} md={12} lg={12}>

                  <Grid container spacing={3}>
                    {weeklyPrice &&
                      weeklyPrice.map(price => (
                        <Grid item sm={6} xs={6} md={6} lg={6}>
                          <Button
                            onClick={() =>
                              handlePrice(
                                price.price,
                                price.to_range + "Hrs Weekly",
                                "Weekly Rates",
                                price.from_range,
                                price.to_range
                              )
                            }
                            className={
                              selectedPriceButton &&
                                selectedPriceButton ===
                                price.to_range + "Hrs Weekly"
                                ? classes.selectmonthButton
                                : classes.monthButton
                            }
                            variant="contained"
                          >
                            {price.to_range + "Hrs Weekly"}
                          </Button>
                        </Grid>
                      ))}

                    {monthlyPrice &&
                      monthlyPrice.map(price => (
                        <Grid item lg={6} sm={6} xs={6} md={6}>
                          <Button
                            onClick={() =>
                              handlePrice(
                                price.price,
                                price.to_range + "Hrs Monthly",
                                "Monthly Rates",
                                price.from_range,
                                price.to_range

                              )
                            }
                            className={
                              selectedPriceButton &&
                                selectedPriceButton ===
                                price.to_range + "Hrs Monthly"
                                ? classes.selectmonthButton
                                : classes.monthButton
                            }
                            variant="contained"
                          >
                            {price.to_range + "Hrs Monthly"}
                          </Button>
                        </Grid>
                      ))}

                    {/* {selectedPriceButton ? (
                    // <Grid>
                    <Button
                      className={classes.totalAmount}
                      variant="contained"
                      color="primary"
                    >

                      Total Amount:
                      <TextField
                        style={{ textColor: "#ffff" }}
                        className={classes.textField}
                        InputProps={{
                          className: classes.input
                        }}
                        onChange={onHandlePrice}
                        type="number"
                        id="standard-size-small"
                        value={selectedPrice}
                        size="small"
                      />
                    </Button>
                  ) : (
                      ""
                    )} */}
                  </Grid>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>

                  <Grid container spacing={3}>
                    {hourlyPrice &&
                      hourlyPrice.map(price => (
                        <Grid item sm={6} xs={6} md={6} lg={6}>
                          <Button
                            onClick={() =>
                              handleHourlyPrice(
                                price.price,
                                price.from_range + "-" + price.to_range + " Hrs",
                                "Hourly Rates",
                                price.from_range,
                                price.to_range
                              )
                            }
                            className={
                              selectedCls &&
                                selectedCls ===
                                price.from_range + "-" + price.to_range + " Hrs"
                                ? classes.selectmonthButton
                                : classes.monthButton
                            }
                            variant="contained"
                          >
                            {price.from_range + "-" + price.to_range + " Hrs"}
                          </Button>
                        </Grid>
                      ))}

                    {dailyPrice &&
                      dailyPrice.map(price => (
                        <Grid item lg={6} sm={6} xs={6} md={6}>
                          <Button
                            onClick={() =>
                              handleDailyPrice(
                                price.price,
                                price.to_range + "Hrs. Daily",
                                "Daily Rates",
                                price.from_range,
                                price.to_range

                              )
                            }
                            className={
                              selectedCls &&
                                selectedCls ===
                                price.to_range + "Hrs. Daily"
                                ? classes.selectmonthButton
                                : classes.monthButton
                            }
                            variant="contained"
                          >
                            {price.to_range + "Hrs. Daily"
                            }
                          </Button>
                        </Grid>
                      ))}

                  </Grid>
                </Grid>


                <Grid lg={12} sm={12} xs={12} md={12}>
                  <Grid container spacing={3}>
                    {
                      selectedPriceButton &&
                      <>
                        <Grid item sm={12} lg={12} xs={12} md={12}>
                          <FormControl fullWidth variant="outlined" className={classes.formControl}>
                            <InputLabel id="demo-simple-select-outlined-label">No of Month</InputLabel>
                            <Select
                              labelId="demo-simple-select-outlined-label"
                              id="demo-simple-select-outlined"
                              value={noOfMonth}
                              onChange={handleNoOfMonth}
                              label="No Of Month"
                            >
                              <MenuItem value={1}>1</MenuItem>
                              <MenuItem value={2}>2</MenuItem>
                              <MenuItem value={3}>3</MenuItem>
                              <MenuItem value={4}>4</MenuItem>
                              <MenuItem value={5}>5</MenuItem>
                              <MenuItem value={6}>6</MenuItem>
                              <MenuItem value={7}>7</MenuItem>
                              <MenuItem value={8}>8</MenuItem>
                              <MenuItem value={9}>9</MenuItem>
                              <MenuItem value={10}>10</MenuItem>
                              <MenuItem value={11}>11</MenuItem>
                              <MenuItem value={11}>12</MenuItem>

                            </Select>
                          </FormControl>
                        </Grid>

                        <Grid spacing={3} item lg={12} sm={12} xs={12} md={12} >

                          <FormControlLabel
                            control={
                              <OtoparkCheckBox
                                checked={entryStatus}
                                onChange={handleEntryStatus}
                                name="checkedB"
                                color="primary"
                              />
                            }
                            label="Make Entry"
                          />
                        </Grid>
                        <Grid spacing={3} item lg={12} sm={12} xs={12} md={12} >
                          <TextField type="number"
                            color="secondary" value={discount} onChange={handleDiscount} fullWidth label="Discount Amount" variant="outlined" />
                        </Grid>
                      </>

                    }
                    {
                      !selectedPriceButton &&
                      <>
                        {
                          organisation &&
                          <>
                            <Grid item sm={12} lg={12} xs={12} md={12}>
                              <FormControl fullWidth variant="outlined" className={classes.formControl}>
                                <InputLabel id="demo-simple-select-outlined-label">Select Organisation</InputLabel>
                                <Select
                                  labelId="demo-simple-select-outlined-label"
                                  id="demo-simple-select-outlined"
                                  value={selectedOrganisation}
                                  onChange={handleOrganisationChange}
                                  label="Select Organisation"
                                >
                                  <MenuItem value={0}>Select Oranisation</MenuItem>
                                  {
                                    organisation.map((obj) => (
                                      <MenuItem value={obj.name}>{obj.name.toUpperCase()}</MenuItem>

                                    ))
                                  }
                                </Select>
                              </FormControl>
                            </Grid>
                            {
                              selectedOrganisation != 0 &&
                              <Grid spacing={3} item lg={12} sm={12} xs={12} md={12} >
                                <FormControlLabel
                                  control={
                                    <OtoparkCheckBox
                                      checked={orgPassStatus}
                                      onChange={handleOrgPassStatusChange}
                                      name="checkedB"
                                      color="primary"
                                    />
                                  }
                                  label="Make Pass"
                                />
                              </Grid>
                            }
                          </>

                        }

                        <Grid classes={classes.spacing} spacing={3} item sm={12} xs={12} md={12} >

                          <TextField type="number"
                            color="secondary" value={advance} type="number" onChange={handleAdvance} fullWidth label="Advance Amount" variant="outlined" />
                        </Grid>

                      </>

                    }
                    {
                      selectedPriceButton ?
                        <Grid item sm={12} lg={12} xs={12} md={12} >
                          <TextField color="secondary" fullWidth value={total} label="Total Amount" disabled={true} variant="outlined" />
                        </Grid>
                        :
                        <Grid item sm={12} lg={12} xs={12} md={12} >
                          <TextField color="secondary" fullWidth value={total} label="Total Amount" disabled={true} variant="outlined" />
                        </Grid>



                    }

                  </Grid>
                </Grid>

                {advance > 0 ?

                  <div
                    onClick={() => proceedPay("ADVANCE")}
                    className={classes.printTicket}
                    variant="contained"
                  >
                    <p className={classes.printTicketText}>Pay Now</p>
                  </div>
                  : selectedPriceButton ? (
                    <div
                      onClick={() => proceedPay("PAY")}
                      className={classes.printTicket}
                      variant="contained"
                    >
                      <p className={classes.printTicketText}>Pay Now</p>
                    </div>
                  ) : hourOrDailyActive ? (
                    <div
                      onClick={() => proceedPay("GETIN")}
                      className={classes.printTicket}
                      variant="contained"
                    >
                      <p className={classes.printTicketText}>GET IN</p>
                    </div>
                  ) : (
                        ""
                      )}


              </Grid>
              <Tab />
            </Grid>
          </Grid>
          <Tab />
        </Grid>
      </div>
    </>
  );
}


const OtoparkCheckBox = withStyles({
  root: {
    color: "#00BBDC",
    '&$checked': {
      color: "#00BBDC",
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);