import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import { Card, CardContent, Grid, InputBase } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Fab from "@material-ui/core/Fab";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Zoom from "@material-ui/core/Zoom";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import SearchIcon from "@material-ui/icons/Search";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { Link, Redirect } from "react-router-dom";
import { useStyles } from "./css";
import {
  getBookingReport,
  getBookingReportByVRNumber,
  getMoreBookingReport,
  makeVehicleExit
} from "../../../Service";
import {
  Challan,
  ChallanPaymentTypeModal,
  Modal,
  TransactionReport
} from "../../../Modal/Modal";
import Loader from "../../../util/Loader/Loader";
import LoginModal from "../../../util/LoginModal/LoginModal";

var moment = require("moment");

function ScrollTop(props) {
  const { children, window } = props;
  const classes = useStyles();
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: -1
  });

  const handleClick = event => {
    const anchor = (event.target.ownerDocument || document).querySelector(
      "#back-to-top-anchor"
    );

    if (anchor) {
      anchor.scrollIntoView({ behavior: "smooth", block: "center" });
    }
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role="presentation" className={classes.root}>
        {children}
      </div>
    </Zoom>
  );
}

ScrollTop.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func
};

export default function OutReport(props) {
  const classes = useStyles();
  const [VRNumber, setVRNumber] = useState("");
  const [redirectToLogin, setRedirectToLogin] = useState(false);
  const [result, setResult] = useState([]);
  const [next, setNext] = useState(null);
  const [openAddChallan, setOpenAddChallan] = useState(false);
  const [openTransactionModal, setOpenTransactionModal] = useState(false);
  const [openPaymentModal, setOpenPaymentModel] = useState(false);
  const [selectedBooking, setSelectedBooking] = useState(null);
  const [showLoader, setShowLoader] = useState(false);
  const [error, setError] = useState(false);
  const [errorDetails, setErrorDetails] = useState(null);
  const [errorTitle, setErrorTitle] = useState(null);

  useEffect(() => {
    getBookingRepo();
  }, []);

  function getBookingRepo() {
    setShowLoader(true);
    let payload = sessionStorage.getItem("payload");
    if (!payload) {
      setRedirectToLogin(true);
      return;
    }
    payload = JSON.parse(payload);

    getBookingReport(payload.host, payload.parking_table_id, payload.ranger_id)
      .then(res => {
        setResult(res.data.results);
        setNext(res.data.links.next);
        setShowLoader(false);
      })
      .catch(err => {
        console.log(err);
      });
  }

  function getBookingRepoByVRN(vrn) {
    // setShowLoader(true);
    let payload = sessionStorage.getItem("payload");
    if (!payload) {
      setRedirectToLogin(true);
      return;
    }
    payload = JSON.parse(payload);
    getBookingReportByVRNumber(
      payload.host,
      payload.parking_table_id,
      payload.ranger_id,
      vrn
    )
      .then(res => {
        setResult(res.data.results);
        setNext(res.data.links.next);
        // setShowLoader(false);
      })
      .catch(err => {
        console.log(err);
      });
  }

  function getMoreResult() {
    // setShowLoader(true);
    getMoreBookingReport(next)
      .then(res => {
        setResult([...result, ...res.data.results]);
        setNext(res.data.links.next);
        // setShowLoader(false);
      })
      .catch(err => {
        console.log(err);
      });
  }

  if (redirectToLogin) {
    return <Redirect to="/otopark/executive/login" />;
  }

  function handleVRNumberChange(e) {
    let vrn = e.target.value;
    if (vrn.length > 0 && vrn.length <= 2) {
      return;
      // getBookingRepo();
    } else if (vrn.length > 2) {
      getBookingRepoByVRN(vrn.toUpperCase());
    } else {
      getBookingRepo();
    }
    setVRNumber(vrn.toUpperCase());
  }

  function viewTransaction(payload) {
    console.log("dd");
    setSelectedBooking(payload);

    setOpenTransactionModal(true);
  }
  function addChallan(payload) {
    setSelectedBooking(payload);
    setOpenAddChallan(true);
  }
  function handleCloseCallan() {
    setOpenAddChallan(false);
  }
  function handleTransactionReport() {
    setOpenTransactionModal(false);
  }
  function proceedToPay(value) {
    console.log(value);
    setOpenPaymentModel(true);
  }
  function proceedPayment(type) {
    if (type === "CASH") {
    } else {
    }
  }
  function handlePaymentModalClose() {
    setOpenPaymentModel(false);
  }

  function makeExit(obj) {
    let payload = sessionStorage.getItem("payload");
    if (!payload) {
      setRedirectToLogin(true);
      return;
    }
    payload = JSON.parse(payload);

    makeVehicleExit(
      payload.host,
      payload.parking_table_id,
      payload.ranger_id,
      obj.id
    )
      .then(res => {
        if (res.data.status === "success") {
          setError(true);
          setErrorTitle("Success");
          setErrorDetails(res.data.message);
          getBookingRepo();
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  function handleOpenModal() {
    setError(true);
  }
  function handleCloseModal() {
    setError(false);
  }

  return (
    <>
      {error && (
        <LoginModal
          isOpen={error}
          openModal={handleOpenModal}
          closeModal={handleCloseModal}
          message={errorDetails}
          title={errorTitle}
        />
      )}
      {showLoader ? (
        <Loader />
      ) : (
        <div className={classes.report}>
          {openAddChallan && (
            <Challan
              booking={selectedBooking}
              handleCloseCallan={handleCloseCallan}
              openChallan={openAddChallan}
              proceedToPay={proceedToPay}
            />
          )}
          {openTransactionModal && (
            <TransactionReport
              booking={selectedBooking}
              handleCloseTransactionReport={handleTransactionReport}
              openTransactionReport={openTransactionModal}
            />
          )}
          {openPaymentModal && (
            <ChallanPaymentTypeModal
              handleClose={handlePaymentModalClose}
              paymentModal={openPaymentModal}
              proceedPayment={proceedPayment}
            />
          )}
          <React.Fragment>
            <div style={{ width: "100%" }}>
              <AppBar position="static" style={{ backgroundColor: "#00BBDC" }}>
                <Toolbar>
                  <Link to="/otopark/executive/getmore">
                    <IconButton
                      edge="start"
                      className={classes.menuButton}
                      color="inherit"
                      aria-label="open drawer"
                    >
                      <ArrowBackIcon
                        className={classes.desktopView}
                        style={{ color: "white" }}
                      />
                    </IconButton>
                  </Link>
                  <div className={classes.search}>
                    <div className={classes.searchIcon}>
                      <SearchIcon />
                    </div>
                    <InputBase
                      placeholder="Search By Vehicle No"
                      classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput
                      }}
                      // value={VRNumber}
                      onChange={handleVRNumberChange}
                      inputProps={{ "aria-label": "search" }}
                    />
                  </div>
                </Toolbar>
              </AppBar>
            </div>
            <Toolbar id="back-to-top-anchor" />
            <Container>
              <Box my={2}>
                <Grid item xs={12}>
                  <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                  >
                    {result.map(obj => {
                      return (
                        <>
                          <Card className={classes.root}>
                            <CardContent>
                              <Grid container>
                                <Grid item lg={12}>
                                  <div className={classes.content}>
                                    <p style={{ fontSize: "15px" }}>
                                      {/* <b>BOOKING ID  </b>: {obj.booking} */}
                                    </p>
                                    <p>
                                      <b>VEHICLE NO </b>: {obj.VRNumber}
                                    </p>
                                    <p>
                                      <b>VEHICLE TYPE </b>: {obj.vehicle_type}
                                    </p>
                                    <p>
                                      <b>ENTRY TIME</b>: {obj.entryTime_new}
                                    </p>
                                    {obj.exitTime_new && (
                                      <p>
                                        {" "}
                                        <b>EXIT TIME</b>: {obj.exitTime_new}
                                      </p>
                                    )}
                                    <p>
                                      <b>DURATION FROM NOW </b>:{" "}
                                      {obj.exitTime_new
                                        ? moment(
                                            obj.exitTime_new,
                                            "DD-MM-YYYY kk:mm"
                                          ).fromNow()
                                        : moment(
                                            obj.entryTime_new,
                                            "DD-MM-YYYY kk:mm"
                                          ).fromNow()}
                                    </p>
                                    {obj.exitTime_new ? (
                                      <p>
                                        <b>VEHICLE STATUS</b>: OUT
                                      </p>
                                    ) : (
                                      <p>
                                        <b>VEHICLE STATUS</b>: IN
                                      </p>
                                    )}
                                  </div>
                                </Grid>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                  <ButtonGroup
                                    size="large"
                                    color="primary"
                                    aria-label="large outlined primary button group"
                                  >
                                    {obj.exitTime_new ? (
                                      <Button
                                        className={classes.viewTransactionBtn}
                                        onClick={() => viewTransaction(obj)}
                                      >
                                        View Transaction Details
                                      </Button>
                                    ) : (
                                      <Button onClick={() => addChallan(obj)}>
                                        Add Challan
                                      </Button>
                                    )}
                                  </ButtonGroup>
                                </Grid>

                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                  <br />
                                  <ButtonGroup
                                    size="large"
                                    color="primary"
                                    aria-label="large outlined primary button group"
                                  >
                                    {obj.exitTime_new == null && (
                                      <Button
                                        aria-label="large outlined primary button"
                                        onClick={() => makeExit(obj)}
                                      >
                                        MAKE EXIT
                                      </Button>
                                    )}
                                  </ButtonGroup>
                                </Grid>
                              </Grid>
                            </CardContent>
                          </Card>
                        </>
                      );
                    })}
                  </Grid>
                  <br />
                  <br />
                </Grid>
              </Box>
              {next && (
                <Button onClick={getMoreResult} className={classes.viewMoreBtn}>
                  View More
                </Button>
              )}
            </Container>
            <ScrollTop {...props}>
              <Fab
                color="secondary"
                size="small"
                aria-label="scroll back to top"
                style={{ backgroundColor: "#00BBDC", marginTop: "-60px" }}
              >
                <KeyboardArrowUpIcon style={{ color: "#fff" }} />
              </Fab>
            </ScrollTop>
          </React.Fragment>
        </div>
      )}
    </>
  );
}

export const TransactionDetails = function TransactionDetails() {
  return <></>;
};
