import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
   
    [theme.breakpoints.down("sm")]: {
    //   padding: "30px"
    }
  },
  desktopView: {
    display: "none",

    [theme.breakpoints.down("sm")]: {
      display: "block",
      padding:"20px"
    },
    
  }
  ,textField:{

    [theme.breakpoints.down("sm")]: {
        // width:'150px',
      },

}
}));
