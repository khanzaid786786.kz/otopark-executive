import React, { useState, useEffect } from "react";
import {
  Divider,
  Grid,
  List,
  ListItem,
  ListItemText,
  TextField
} from "@material-ui/core";
import { useStyles } from "./css";
import Button from "@material-ui/core/Button";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Link } from "react-router-dom";
import { getTotalCollection } from "../../Service";
import Loader from "../../util/Loader/Loader";

var moment = require("moment");

export default function FullWidthGrid() {
  const classes = useStyles();
  const [startDate, setStartDate] = useState(new Date());
  const [summaryDate, setSummaryDate] = useState(
    moment(new Date()).format("DD/MM/YYYY")
  );
  const [details, setDetails] = useState(null);
  const [showLoader, setShowLoader] = useState(true);

  function handleDate(date) {
    // console.log(moment(date).format("DD-MM-YYYY HH:mm"));
    setStartDate(date);
    setSummaryDate(moment(date).format("DD/MM/YYYY"));
    let payload = JSON.parse(sessionStorage.getItem("payload"));
    let request = {
      access_token: payload.access_token,
      ranger_id: payload.ranger_id,
      parking_table_id: payload.parking_table_id,
      host: payload.host,
      summaryDate: moment(date).format("DD/MM/YYYY")
    };
    setShowLoader(true);
    getTotalCollection(request)
      .then(res => {
        console.log(res);
        if (res.data.status === "success") {
          setDetails(res.data);
          setShowLoader(false);
        } else {
          setShowLoader(false);
          alert(res.data.message);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  useEffect(() => {
    setShowLoader(true);

    let payload = JSON.parse(sessionStorage.getItem("payload"));
    let request = {
      access_token: payload.access_token,
      ranger_id: payload.ranger_id,
      parking_table_id: payload.parking_table_id,
      host: payload.host,
      summaryDate: summaryDate
    };
    getTotalCollection(request)
      .then(res => {
        if (res.data.status === "success") {
          setDetails(res.data);
          setShowLoader(false);
        } else {
          setShowLoader(false);
          alert(res.data.message);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  function printStatement() {
    try {
      let payload = JSON.parse(sessionStorage.getItem("payload"));

      details &&
        window.Android.printSummaryReceipt(
          details.message.TOTAL,
          details.message.CASH,
          details.message.CARD,
          details.message.UPI,
          details.message.FASTTAGS,
          summaryDate,
          payload.parking_name,
          payload.parking_address
        );
    } catch (error) {
      console.log(error);
    }
  }

  const ExampleCustomInput = ({ value, onClick }) => (
    <button
      className="example-custom-input btn-block"
      onClick={onClick}
      style={{
        width: "80%",
        marginRight: "5px",
        // padding: " 3px 3px 3px 3px",
        fontSize: "15px",
        zIndex: "9999",
        backgroundColor: "#fff",
        fontWeight: "bold",
        color: "#00bbdc",
        borderBottom: "2px solid #00bbdc",
        borderLeft: "2px solid #00bbdc",
        borderRight: "2px solid #00bbdc",
        borderTop: "2px solid #00bbdc"
      }}
    >
      <i
        style={{ padding: "5px" }}
        class="fa fa-calendar"
        aria-hidden="true"
      ></i>
      {/* {!value ? "Select Date" : value} */}
      {value}
    </button>
  );

  return (
    <>
      {showLoader ? (
        <Loader />
      ) : (
        <div className={classes.root}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              sm={12}
              md={12}
              lg={12}
              style={{
                backgroundColor: "#00BBDC",
                width: "100%",
                marginTop: "10px"
              }}
            >
              <div style={{ marginLeft: "20px", marginTop: "20px" }}>
                <div className={classes.desktopView}>
                  <Link to="/otopark/executive/getmore">
                    <ArrowBackIcon style={{ color: "white" }} />
                  </Link>
                </div>
                <div
                  className="customDatePickerWidth"
                  className={classes.datePicker}
                >
                  <DatePicker
                    selected={startDate}
                    onChange={date => handleDate(date)}
                    dateFormat="dd/MM/yyyy"
                    closeOnScroll={true}
                    customInput={<ExampleCustomInput />}
                    withPortal
                    popperModifiers={{
                      preventOverflow: {
                        enabled: true
                      }
                    }}
                  />
                </div>
                <br />
                <br />
                <small style={{ color: "white" }}>Total Revenue</small>
                <h2 style={{ color: "white", fontSize: "20px" }}>
                  &#8377;
                  {details && details.message.TOTAL}
                </h2>
                <p
                  style={{
                    color: "white",
                    fontSize: "10px"
                    // marginTop: "-15px"
                  }}
                ></p>
                <div className={classes.breakDiv}></div>
              </div>

              <br />
              <br />

              <Grid container spacing={2}>
                <div className={classes.bottomdiv}>
                  <List component="nav" aria-label="main mailbox folders">
                    <ListItem>
                      <ChevronLeftIcon style={{ color: "#00BBDC" }} />
                      <ListItemText primary="Cash Collection" />
                      <ListItemText
                        align="right"
                        primary={details && "Rs. " + details.message.CASH}
                      />
                    </ListItem>
                    <Divider />
                    <ListItem>
                      <ChevronLeftIcon style={{ color: "#00BBDC" }} />
                      <ListItemText primary="Card Collection" />
                      <ListItemText
                        align="right"
                        primary={details && "Rs. " + details.message.CARD}
                      />
                    </ListItem>
                    <Divider />
                    <ListItem>
                      <ChevronLeftIcon style={{ color: "#00BBDC" }} />
                      <ListItemText primary="UPI Collection" />
                      <ListItemText
                        align="right"
                        primary={details && "Rs. " + details.message.UPI}
                      />
                    </ListItem>
                    <Divider />
                    <ListItem>
                      <ChevronLeftIcon style={{ color: "#00BBDC" }} />
                      <ListItemText primary="Payements by FASTTag" />
                      <ListItemText
                        align="right"
                        primary={details && "Rs. " + details.message.FASTTAGS}
                      />
                    </ListItem>
                    <Button
                      onClick={printStatement}
                      className={classes.printStatementBtn}
                    >
                      Print Statement
                    </Button>
                    <br />
                    <br />
                  </List>
                </div>
              </Grid>
              {/* end */}
            </Grid>
          </Grid>
        </div>
      )}
    </>
  );
}
