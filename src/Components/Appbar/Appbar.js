import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { useStyles } from "./css";
import { Redirect } from "react-router-dom";

export default function ButtonAppBar() {
  const classes = useStyles();
  const [payload, setPayload] = useState(null);
  const [redirectToLogin, setRedirectToLogin] = useState(false);

  useEffect(() => {
    let payload = sessionStorage.getItem("payload");
    if (!payload) {
      setRedirectToLogin(true);
      return;
    }
    payload = JSON.parse(payload);
    if (!payload) {
      setRedirectToLogin(true);
      return;
    }
    setPayload(payload);
  }, []);

  function logout() {
    sessionStorage.clear();
    setRedirectToLogin(true);
  }

  if (redirectToLogin) {
    return <Redirect to="/otopark/executive/login" />;
  }

  return (
    <div className={classes.root}>
      <AppBar
        className={classes.mobileAppbar}
        position="static"
        style={{
          backgroundColor: "#fff",
          color: "black"
        }}
      >
        <Toolbar>
          <img
            style={{ borderRadius: "50%" }}
            src={payload && payload.avator}
            width="50px"
            height="50px"
          />
          <Typography variant="h9" className={classes.title}>
            <p className={classes.name}>
              {payload && payload.first_name + " " + payload.last_name}
            </p>
            <p className={classes.nameId}>
              Parking Name: {payload && payload.parking_name}
            </p>
          </Typography>
          <div>
            <Button
              onClick={e => logout()}
              className={classes.logoutButton}
              color="inherit"
            >
              Logout
            </Button>
            <img
              src={require("../../assets/icons/logout.svg")}
              width="15px"
              height="15px"
              className={classes.logoutIcon}
            />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
