import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
    minWidth: "100%"
  },
  showPointer: {
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  },
  flexContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexFlow: "wrap"
  },
  transactionText: {
    width: "50%",
    height: "30px",
    // backgroundColor:"red",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  transactionVal: {
    width: "50%",
    height: "30px",
    // backgroundColor:"green",
    justifyContent: "center",
    alignItems: "center"
  },
  txt: {
    fontSize: "15px",
    textAlign: "center"
  },
  printButton: {
    textTransform: "none",
    width: "100px",
    margin: "10px",
    padding: "10px",
    backgroundColor: "#00BBDC",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#00BBDC",
      border: "1px solid #00BBDC"
    }
  },
  closeButton: {
    color: "#fff",
    "&:hover": {
      backgroundColor: "#00BBDC",
      color: "#fff"
    }
  }
}));
