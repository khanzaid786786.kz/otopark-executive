import { makeStyles, fade } from "@material-ui/core/styles";
import { borderColor } from "@material-ui/system";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
    // padding: "10px"
  },
  bottomdiv: {
    width: "100%",
    backgroundColor: "white",
    border: "1px solid lightgrey",
    borderRadius: "10px 10px 0px 0px ",
    marginBottom: "-20px",
    height: "auto",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      height: "auto",
      borderBottom: "none"
    }
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  datePicker: {
    marginLeft: "200px",
    marginTop: "-10px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "150px",
      marginTop: "-30px"
    }
  },
  calender: {
    width: "80%",
    color: "#00BBDC",
    border: "1px solid white",
    backgroundColor: "white",
    color: "#00BBDC",
    borderRadius: "20px",
    marginTop: "0px"
  },
  textField: {
    border: "none",
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  printStatementBtn: {
    marginTop: "30px",
    marginLeft: "90px",
    textTransform: "none",
    width: "60%",
    backgroundColor: "#00BBDC",
    color: "white",
    borderRadius: "20px",
    "&:hover": {
      backgroundColor: "#00BBDC",
      color: "white"
    },
    [theme.breakpoints.down("sm")]: {
      marginLeft: "70px"
    }
  },
  breakDiv: {
    // marginTop: "40px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "220px"
    }
  },
  desktopView: {
    display: "none",
    [theme.breakpoints.down("sm")]: {
      display: "block"
    }
  }
}));
