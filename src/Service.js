import axios from "axios";

export const URL = "http://www.otopark.in:8000/api/"; // PROD URL

export const ENDPOINTS={
    BASE_URL:"https://www.otopark.in:8000/api/",
    // BASE_URL:"http://192.168.1.221:8000/api/", // local 
    
    // BASE_URL:"http://192.168.1.150/api/", // goregoan
    // BASE_URL:"http://192.168.1.119/api/", // aurangabad
    // BASE_URL:"http://192.168.1.221:8000/api/", // home
    
     
    USER_LOGIN:"ranger/rangerLogin/",
    // BASE_WS_URL:"ws://192.168.1.221:8000/",
    // BASE_WS_URL:"ws://192.168.1.150/", //goregoan
    // BASE_WS_URL:"ws://192.168.1.119/", //aurangabad
    // BASE_WS_URL:"ws://192.168.1.221:8000/", //home
      BASE_WS_URL:"ws://www.otopark.in:8000/",
    GET_IN_STREAM : "getIn/stream/",
    GET_OUT_STREAM:"getOut/stream/",
    ANALYTICS:"analytics/stream/",
    GET_SUMMARY:"ranger/getDayCollection/",
    GET_BOOKING_REPORT:"ranger/getBookingReport/",
    GET_BOOKING_REPORT_BY_VRNUMBER:"ranger/getBookingReportByVRNumber/",
    MAKE_EXIT:"ranger/makeExit/",
    GET_TRANSACTION_DETAILS:"ranger/getTransactionDetails/",
}

export const LoginPage = async body => {
  return await axios.post(ENDPOINTS.BASE_URL + ENDPOINTS.USER_LOGIN, body, {
    timeout: 3 * 60 * 1000
  });
};

function convertToFormData(request) {
  const formData = new FormData();

  Object.entries(request).forEach(([key, value]) => {
    formData.append(key, value);
  });
  return formData;
}

export const getTotalCollection = async body => {
  let formData = convertToFormData(body);
  return await axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.GET_SUMMARY,
    formData,
    {
      timeout: 3 * 60 * 1000
    }
  );
};

export const getBookingReport = async (host, parkingTableId, rangerId) => {
  return await axios.get(
    ENDPOINTS.BASE_URL +
      ENDPOINTS.GET_BOOKING_REPORT +
      rangerId +
      "/" +
      host +
      "/" +
      parkingTableId +
      "?limit=200",
    {
      timeout: 3 * 60 * 1000,
      "Content-Type": "application/json"
    }
  );
};

export const getBookingReportByVRNumber = async (
  host,
  parkingTableId,
  rangerId,
  vrn
) => {
  return await axios.get(
    ENDPOINTS.BASE_URL +
      ENDPOINTS.GET_BOOKING_REPORT_BY_VRNUMBER +
      rangerId +
      "/" +
      host +
      "/" +
      parkingTableId +
      "/" +
      vrn +
      "?limit=200",
    {
      timeout: 3 * 60 * 1000,
      "Content-Type": "application/json"
    }
  );
};

export const getMoreBookingReport = async url => {
  return await axios.get(url, {
    timeout: 3 * 60 * 1000,
    "Content-Type": "application/json"
  });
};

export const makeVehicleExit = async (
  host,
  parkingTableId,
  rangerId,
  entry_id
) => {
  return await axios.get(
    ENDPOINTS.BASE_URL +
      ENDPOINTS.MAKE_EXIT +
      rangerId +
      "/" +
      host +
      "/" +
      parkingTableId +
      "/" +
      entry_id,
    {
      timeout: 3 * 60 * 1000,
      "Content-Type": "application/json"
    }
  );
};

export const getTransactionDetails = async (
  host,
  parkingTableId,
  rangerId,
  booking_id
) => {
  return await axios.get(
    ENDPOINTS.BASE_URL +
      ENDPOINTS.GET_TRANSACTION_DETAILS +
      rangerId +
      "/" +
      host +
      "/" +
      parkingTableId +
      "/" +
      booking_id,
    {
      timeout: 3 * 60 * 1000,
      "Content-Type": "application/json"
    }
  );
};
