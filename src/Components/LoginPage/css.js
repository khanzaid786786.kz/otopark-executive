import { makeStyles } from "@material-ui/core/styles";
import shadows from "@material-ui/core/styles/shadows";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: "500px",
    height: "100vh",
    borderRadius:'12px',
    backgroundColor: "#f9f9f9",
    boxShadow: "#2C28281C",
    "& label.Mui-focused": {
      color: "lightgray"
    },
    '& .MuiOutlinedInput-inputAdornedStart':{
      height: "40px",
      fontSize:'20px',
      fontWeight:'700'
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#00BBDC"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "lightgray"
      },
      "&:hover fieldset": {
        borderColor: "lightgray"
      },
      "&.Mui-focused fieldset": {
        borderColor: "lightgray"
      }
    }
  },
  container: {
    minHeight: "100vh", // height of the browser viewport
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minWidth:'100%'
  },
  paper: {
    // marginTop: theme.spacing(),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",

  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%",
    paddingLeft:'30px',
    paddingRight:'30px'
    // Fix IE 11 issue.
    // marginTop: theme.spacing(1)
  },
  submit: {
    textTransform: "none",
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#00BBDC",
    borderRadius: 50,
    width: "50%",
    marginBottom:'50px',
    padding:'10px',
    marginLeft: "25%",
    fontSize:'18px',
    "&:hover": {
      background: "#fff",
      color: "#00BBDC",
      border: "2px solid #00BBDC"
    }
  },
  forgotcheck: {
    display: "flex",
    justifyContent: "space-between"
  },
  textbox: {
    color: "#1717172E",
    backgroundColor: "lightgrey",
    "&:hover": {
      color: "#1717172E"
    }
  },
  loginImage: {
    width: "194px",
    height: "130px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  }
}));
