import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginBottom: "80px",
    padding: "10px",
    [theme.breakpoints.down("sm")]: {
      padding: "10px",
      marginBottom: "60px"
    },
    "& label.Mui-focused": {
      color: "lightgray"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "lightgray"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "lightgray"
      },
      "&:hover fieldset": {
        borderColor: "lightgray"
      },
      "&.Mui-focused fieldset": {
        borderColor: "lightgray"
      }
    }
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  containerDiv: {
    marginLeft: "40px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "60px",
      marginLeft: "10px"
    }
  },
  horizontalLine: {
    borderBottom: "1px solid lightgrey",
    // marginTop: "120px",
    marginBottom: "20px",
    [theme.breakpoints.down("sm")]: {
      borderBottom: "0px solid grey",
      display: "none"
    }
  },
  
  verticalLine: {
    borderRight: "1px solid lightgrey",
    height: "520px",
    // marginTop: "-620px",
    [theme.breakpoints.down("sm")]: {
      borderRight: "0px solid grey",
      marginTop: "-520px"
    }
  },
  vehicleImg: {
    width: "80px",
    height: "60px",
    border: "2px solid black",
    borderRadius: "10px",
    textAlign: "center",
    [theme.breakpoints.down("sm")]: {
      marginBottom: "20px"
    }
  },
  datePicker: {
    marginRight: "0px",
    [theme.breakpoints.down("sm")]: {
      marginRight: "0px"
    }
  },
  vehicleInput: {
    backgroundColor: "#F7F7F7",
    width: "60%",
    color: "red",
    fontSize: "50px",
    [theme.breakpoints.down("sm")]: {
      width: "80%"
    }
  },
  resize: {
    fontWeight: "bold",
    fontSize: 20
  },
  vehicleInputLabel: {
    width: "60%",
    [theme.breakpoints.down("sm")]: {
      display: "none",
      width: "80%"
    }
  },
  vehicleInputOwnerNumber: {
    // backgroundColor: "#F0F5F7",
    // width: "60%",
    [theme.breakpoints.down("sm")]: {
      marginTop: "-80px"
    }
  },
  totalAmount: {
    marginLeft:"40px",
    width: "80%",
    height: "50px",
    marginTop: "40px",
    backgroundColor: "red",
    fontWeight:'700',
    "&:hover": {
      backgroundColor: "red",
      color: "white"
    },
    [theme.breakpoints.down("sm")]: {
      width: "80%",
      height: "60px",
      marginTop: "0px",
      padding:'40px',
      borderRadius: "10px",
      textTransform: "none",
      fontSize: "15px",
      // marginLeft: "-8px",
      
      // marginTop: "-150px"
    }
  },

  choosenVehicle: {
    width: "80px",
    height: "60px",
    border: "2px solid black",
    borderRadius: "10px",
    textAlign: "center",
    cursor: "pointer",
    backgroundColor: "#00BBDC"
  },
  unchoosenVehicle: {
    width: "80px",
    height: "60px",
    cursor: "pointer",
    border: "2px solid black",
    borderRadius: "10px",
    textAlign: "center"
  },
  selectmonthButton: {
    backgroundColor: "#00BBDC",
    color: "white",
    textTransform: "none",
    width: "80%",
    [theme.breakpoints.down("sm")]: {
      marginTop: "10px",
      width: "100%",
      marginBottom: "50px",
      backgroundColor: "##00BBDC !important"
    }
  },
  monthButton: {
    textTransform: "none",
    width: "80%",
    backgroundColor: "#F0F5F7",
    color: "black",
    "&:hover": {
      backgroundColor: "#00BBDC",
      color: "white"
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: "10px",
      width: "100%",
      marginBottom: "50px",
      backgroundColor: "#F0F5F7",
      "&:hover": {
        backgroundColor: "#00BBDC",
        color: "white"
      },
    }
  },
  altmonthButton: {
    textTransform: "none",
    width: "80%",
    backgroundColor: "#F0F5F7",
    color: "black",
    "&:hover": {
      backgroundColor: "#00BBDC",
      color: "white"
    },
    [theme.breakpoints.down("sm")]: {
      width: "90%",
      backgroundColor: "#F0F5F7",
      "&:hover": {
        backgroundColor: "#00BBDC",
        color: "white"
      },
    }
  },

  printTicket: {
    position: "fixed",
    bottom: "1%",
    backgroundColor: "#00BBDC",
    width: "100%",
    height: "50px",
    color: "white",
    marginLeft: "-10px",
    paddingBottom: "5px",
    marginRight: "-12px",
    "&:hover": {
      cursor: "pointer"
    },
    [theme.breakpoints.down("sm")]: {
      position: "relative",
      borderRadius: "50px",
      marginTop: "40px",
      width: "80%",
      marginBottom: "80px",
      display: "block",
      marginLeft: "auto",
      marginRight: "auto",
      
    }
  },
  printTicketText: {
    fontSize: "25px",
    fontWeight: "bold",
    marginLeft: "190px",
    marginTop: "14px",
    [theme.breakpoints.down("sm")]: {
      padding: "15px",
      marginLeft: "70px",
      marginBottom: "80px",
      
    }
  },
  vehicleScrollbar: {
    [theme.breakpoints.down("sm")]: {
      overflowX: "auto",
      whiteSpace: "nowrap"
    }
  },
  gridFour: {
    marginTop: "0px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "20px",
      marginTop: "-150px",
      marginBottom: "100px"
    }
  },
  mobileView: {
    [theme.breakpoints.down("sm")]: {
      marginTop: "20px",
      padding: "0px"
    }
  },
  textField: {
    color: "white",
    fontWeight: 700,
    borderColor: "2px solid red",
    "&&&:before": {
      borderBottom: "none"
    },
    "&&:after": {
      borderBottom: "none"
    },
    [theme.breakpoints.down("sm")]: {
      
    }
  },
  input: {
    color: "#ffff",
    fontWeight:'700',
    fontSize:'24px',
    border:'None',
    "&&&:before": {
      borderBottom: "none"
    },
    "&&:after": {
      borderBottom: "none"
    },
    [theme.breakpoints.down("sm")]: {
     paddingLeft:'20px'
    }
  }
}));
