import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    // padding: "10px",
    "& label.Mui-focused": {
      color: "#00BBDC"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "lightgray"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "lighgray"
      },
      "&:hover fieldset": {
        borderColor: "lightgray"
      },
      "&.Mui-focused fieldset": {
        borderColor: "lightgray"
      }
    }
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  containerDiv: {
    marginLeft: "40px",
    marginBottom:"100px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "70px",
      marginLeft: "20px"
    }
  },
  listItemText: {
    fontSize: "0.7em" //Insert your required size
  },
  horizontalLine: {
    borderBottom: "1px solid grey",
    marginTop: "20px",
    marginBottom: "20px",
    [theme.breakpoints.down("sm")]: {
      borderBottom: "0px solid grey"
    }
  },
  verticalLine: {
    borderRight: "1px solid lightgrey",
    height: "500px",
    marginTop: "-240px",
    [theme.breakpoints.down("sm")]: {
      borderRight: "0px solid grey"
    }
  },
  vehicleImg: {
    width: "80px",
    height: "60px",
    border: "2px solid black",
    borderRadius: "10px",
    textAlign: "center"
  },
  // textField: {
  //   width: 300,
  //   margin: 100
  // },
  resize: {
    fontWeight: "bold",
    fontSize: 20
  },
  vehicleInput: {
    backgroundColor: "#F7F7F7",
    width: "60%",
    fontSize: "24px",
    fontWeight: "bold",
    [theme.breakpoints.down("sm")]: {
      width: "80%"
    }
  },
  datePicker: {
    marginRight: "100px",
    [theme.breakpoints.down("sm")]: {
      marginRight: "0px"
    }
  },
  vehicleInputLabel: {
    width: "60%",
    [theme.breakpoints.down("sm")]: {
      display: "none",
      width: "80%"
    }
  },
  vehicleInputOwnerNumber: {
    backgroundColor: "#F0F5F7",
    width: "60%",
    [theme.breakpoints.down("sm")]: {
      display: "none",
      width: "80%"
    }
  },
  totalAmount: {
    width: "100%",
    marginTop: "20px",
    backgroundColor: "#00BBDC",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      height: "60px",
      borderRadius: "10px",
      textTransform: "none",
      fontSize: "20px",
      marginLeft: "-8px"
      // marginTop: "-150px"
    }
  },
  monthButton: {
    textTransform: "none",
    width: "80%",
    backgroundColor: "#F0F5F7",
    color: "#7A7F85",
    "&:hover": {
      backgroundColor: "#00BBDC",
      color: "white"
    },
    [theme.breakpoints.down("sm")]: {
      width: "90%"
    }
  },
  altmonthButton: {
    textTransform: "none",
    width: "80%",
    backgroundColor: "#F0F5F7",
    color: "#7A7F85",
    "&:hover": {
      backgroundColor: "#00BBDC",
      color: "white"
    },
    [theme.breakpoints.down("sm")]: {
      width: "90%"
    }
  },
  subTotal: {
    // border: "1px solid grey",
    width: "20%",
    [theme.breakpoints.down("sm")]: {
      width: "35%"
    },
    "& label.Mui-focused": {
      color: "#00BBDC"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#00BBDC"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "lightgrey"
      },
      "&:hover fieldset": {
        borderColor: "lightgrey"
      },
      "&.Mui-focused fieldset": {
        borderColor: "lightgrey"
      }
    }
  },
  printTicket: {
    position: "fixed",
    bottom: "1%",
    backgroundColor: "#00BBDC",
    width: "100%",
    height: "50px",
    color: "white",
    marginLeft: "-10px",
    paddingBottom: "5px",
    marginRight: "-12px",
    "&:hover": {
      cursor: "pointer"
    },
    [theme.breakpoints.down("sm")]: {
      position: "relative",
      borderRadius: "50px",
      width: "250px",
      display: "block",
      marginLeft: "auto",
      marginRight: "auto",
      marginBottom:"60px"
    }
  },
  printTicketText: {
    fontSize: "25px",
    fontWeight: "bold",
    marginLeft: "160px",
    marginTop: "12px",
    [theme.breakpoints.down("sm")]: {
      padding: "15px",
      marginLeft: "40px"
    }
  },
  vehicleScrollbar: {
    [theme.breakpoints.down("sm")]: {
      overflowX: "auto",
      whiteSpace: "nowrap"
    }
  },
  gridFour: {
    marginTop: "0px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "20px",
      marginTop: "-300px",
      marginBottom: "100px"
    }
  }
}));
