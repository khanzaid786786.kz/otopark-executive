import React, { useState, useEffect, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import InboxIcon from "@material-ui/icons/Inbox";
import DraftsIcon from "@material-ui/icons/Drafts";
import { useStyles } from "./css";
import Appbar from "../Appbar/Appbar";
import Tab from "../Tab/Tab";
import DatePicker from "react-datepicker";
import { ENDPOINTS } from "../../Service";
import LoginModal from "../../util/LoginModal/LoginModal";

import { Modal,BridgeModalCom, BridgeCardModalCom,PrintModal } from "../../Modal/Modal";

import "react-datepicker/dist/react-datepicker.css";
import { w3cwebsocket as W3CWebSocket } from "websocket";

export default function GetOut() {
  const classes = useStyles();
  const [value, setValue] = useState("");
  const ws = useRef(null);
  const [socketConnection, setSocketConnection] = useState(false)
  const [VRNumber, setVRNumber] = useState("");
  const [ownerName, setOwnerName] = useState("");
  const [contactNo, setContactNo] = useState("");
  const [error, setError] = useState(false);
  const [errorDetails, setErrorDetails] = useState(null);
  const [errorTitle, setErrorTitle] = useState(null);
  const [vehicleType, setVehicleType] = useState(null);
  const [bookingNo, setBookingNo] = useState("");
  const [payNow, setPayNow] = useState(false);
  const [showPaymentModal, setShowPaymentModal] = useState(false);
  const [charges, setCharges] = useState(null);
  const [qrcode, setQrcode] = useState("");
  const [anprData, setAnprData] = useState(null);
  const [anprImage, setAnprImage] = useState(null);
  const [tagId, setTagId] = useState("");
  const [tagMessage, setTagMessage] = useState("No Tags");
  const [openQrCodeModal,setOpenQrCodeModal] =useState(false);
  const [cardPaymentModal,setCardPaymentModal]=useState(false);
  const [cardData,setCardData]=useState(null);
  const [wantReceipt,setWantReceipt]=useState(false);
  const [response,setResponse]=useState(null);

  function handleCloseCardPaymentModal(){
    setCardPaymentModal(false);
  }
  function handleCardData(data){
  setCardData(data);
  }



  useEffect(() => {
    ws.current = new W3CWebSocket(
      ENDPOINTS.BASE_WS_URL + ENDPOINTS.GET_IN_STREAM
    );
    console.log("web socket connected!!");
  }, []);

  if (socketConnection) {
    console.log("Connection Done SuccessFully!!");
  }


  useEffect(() => {
    
    ws.current.onopen = () => {
      console.log("WebSocket Client Connected");
      setSocketConnection(true);
      let payload = JSON.parse(sessionStorage.getItem("payload"));

      ws.current.send(
        JSON.stringify({
          access_token: payload.access_token,
          host: payload.host,
          parking_table_id: payload.parking_table_id,
          ranger_id: payload.ranger_id,
          type: "get_latest_anpr_exit"
        })
      );

    };

    ws.current.onmessage = message => {
      console.log(message.data);
      let response = JSON.parse(message.data);
      if (response.status === "error") {
        setShowPaymentModal(false);
        setError(true);
        setErrorDetails(response.message);
        setErrorTitle("");
        setVRNumber("");
        setOwnerName("");
        setContactNo("");
        setTagId("");
        setTagMessage("No Tag");
        setBookingNo("");
        setQrcode("");
       
      } else if (response.status === "charges") {
        setCharges(response.payload.charges)
        setVehicleType(response.payload.vehicleType)
        setValue(response.payload.charges.charge_amount);
        setTagId(response.payload.tagId?response.payload.tagId:"");
        setContactNo(response.payload.contactNo?response.payload.contactNo:"");
        setBookingNo(response.payload.bookingNo?response.payload.bookingNo:"");
        setOwnerName(response.payload.ownerName?response.payload.ownerName:"");
        setPayNow(true);


        setVRNumber(response.payload.VRNumber);
      } else if (response.status === "exit") {
        setShowPaymentModal(false);
    
        setWantReceipt(true);
        setResponse(response);
        let payload = JSON.parse(sessionStorage.getItem("payload"));

        ws.current.send(
          JSON.stringify({
            access_token: payload.access_token,
            host: payload.host,
            parking_table_id: payload.parking_table_id,
            ranger_id: payload.ranger_id,
            type: "openExit"
          })
        )
      } else if (response.status === "activePass") {
        setShowPaymentModal(false);
        setError(true);
        setErrorDetails(response.message);
        setErrorTitle("");
        setVRNumber("");
        setOwnerName("");
        setContactNo("");
        setQrcode("");
        setTagMessage("No Tags");
        setTagId("");
        setBookingNo("");

        let payload = JSON.parse(sessionStorage.getItem("payload"));

        ws.current.send(
          JSON.stringify({
            access_token: payload.access_token,
            host: payload.host,
            parking_table_id: payload.parking_table_id,
            ranger_id: payload.ranger_id,
            type: "openExit"
          })
        )
        

      } else if (response.message === "anpr") {
        if (response.direction === "Exit") {
          setVRNumber(response.VRNumber);
          setAnprData(response.message);
          setAnprImage(response.cropImageOfNumberPlate);
        }
      }else if(response.status==="organisationVehicle"){
        setShowPaymentModal(false);
        setError(true);
        setErrorDetails(response.message);
        setErrorTitle("");
        setVRNumber("");
        setOwnerName("");
        setContactNo("");
        setQrcode("");
        setTagMessage("No Tags");
        setTagId("");
        setBookingNo("");

      }else if(response.message==="rfid"){
        if(response.direction==="Exit"){
            setTagId(response.tagId);
            setTagMessage(response.tagMessage);
         
        }
      }else if(response.message==="rfidExit"){
        if(response.direction==="Exit"){
          setTagId(response.tagId);
          setTagMessage(response.tagMessage);
          let payload = JSON.parse(sessionStorage.getItem("payload"));
          ws.current.send(
         
          JSON.stringify({
            access_token: payload.access_token,
            host: payload.host,
            parking_table_id: payload.parking_table_id,
            ranger_id: payload.ranger_id,
            type: "openExit"
          })
          );
        }
      }else if(response.message==="rfidError"){
        if(response.direction==="Exit"){
         setTagMessage(response.tagMessage);
         setTagId(response.tagId);
         
        }
      }


    }
  }, [])

  const handleChange = e => {
    console.log(e.target.value);
    setValue(e.target.value);
  };

  function handleTag(e) {
    setTagId(e.target.value);
  }
  function handleOpenModal() {
    setError(true);
  }
  function handleCloseModal() {
    setError(false);
  }

  function printAndHandleClose(){

    setWantReceipt(false);

        setError(true);
        setCharges(false);
        setErrorTitle("Success")
        setErrorDetails("Vehicle Exited SuccessFully!!")
        setVehicleType(null);
        setVRNumber("");
        setOwnerName("");
        setContactNo("");
        setBookingNo("");
        setQrcode("");
        setTagId("");
        setTagMessage("No Tag");
        setBookingNo("");
      
        setPayNow(false);
        
        let payload = JSON.parse(sessionStorage.getItem("payload"));

        try {
          response && window.Android.printReceipt(response.payload.VRNumber, response.payload.entryTime, response.payload.exitTime, payload.parking_address, payload.parking_name, response.payload.vehicleType, payload.host, payload.parking_table_id, payload.ranger_id, response.payload.amount, response.payload.feesType);

        } catch (error) {
          console.log(error)
        }

        
  }
  function handleReceiptClose(){
    setWantReceipt(false);
    setError(true);
        setCharges(false);
        setErrorTitle("Success")
        setErrorDetails("Vehicle Exited SuccessFully!!")
        setVehicleType(null);
        setVRNumber("");
        setOwnerName("");
        setContactNo("");
        setBookingNo("");
        setQrcode("");
        setTagId("");
        setTagMessage("No Tag");
        setBookingNo("");
      
        setPayNow(false);
  }
  
  function handleVRNumber(e) {
    setVRNumber(e.target.value.toUpperCase().replace(" ", ""));
  }

  function handleBookingNo(e) {
    setBookingNo(e.target.value.toUpperCase().replace(" ", ""));
  }

  function proceedPay(type) {
    if(tagId==""|| tagId==null||tagId==undefined)
    if (qrcode == "" || qrcode == null || qrcode == undefined) {
      if (VRNumber == "" || VRNumber == null || VRNumber == undefined) {
        if (bookingNo == "" || bookingNo == null || bookingNo == undefined) {
          setError(true);
          setErrorDetails("Enter Vehicle No Or Booking No Or Scan QrCode Or Tag Id");
          setErrorTitle("Error")
          return;
        }
      }
    }
    if (type === "GETOUT") {
      let payload = JSON.parse(sessionStorage.getItem("payload"));
      ws.current.send(
        JSON.stringify({
          access_token: payload.access_token,
          host: payload.host,
          parking_table_id: payload.parking_table_id,
          ranger_id: payload.ranger_id,
          type: "getOut",
          VRNumber: VRNumber,
          bookingNo: bookingNo,
          qrcode: qrcode,
          anprImage:anprImage?anprImage:"",
          tagId:tagId,
          ownerName:ownerName,
          contactNo:contactNo
          

        })
      );

    } else {
      setShowPaymentModal(true);
    }
  }

  function handleClose() {
    setShowPaymentModal(false);
  }


  function handleQrcode(e) {

    // setQrcode(e.target.value);
    setOpenQrCodeModal(true);
  }

  function handleCloseQrCode(){
    setOpenQrCodeModal(false);
  }

  function proceedPayment(paymentType) {
    if (paymentType === "CASH") {
      let payload = JSON.parse(sessionStorage.getItem("payload"));
      ws.current.send(
        JSON.stringify({
          access_token: payload.access_token,
          host: payload.host,
          parking_table_id: payload.parking_table_id,
          ranger_id: payload.ranger_id,
          type: "takeCharges",
          VRNumber: VRNumber,
          vehicleType: vehicleType,
          paymentType: paymentType,
          feesValue: value,
          tagId:tagId,
          ownerName:ownerName,
          contactNo:contactNo,

          

        })
      );
    } else {
    }
  }

  function handleQrData(value){
   console.log(value);
    setQrcode(value);
    // proceedPay("GETOUT");
  }


  return (


    <div className={classes.root}>
      {wantReceipt && (
        <PrintModal
        handleReceiptClose={handleReceiptClose}
        printAndHandleClose={printAndHandleClose}
          wantReceipt={wantReceipt}
         
        />
      )}
      {cardPaymentModal && (
        <BridgeCardModalCom
          handleClose={handleCloseCardPaymentModal}
          handleCardData={handleCardData}
          open={cardPaymentModal}
          vehicleNo={VRNumber}
          amount = {value}
        />
      )}
      {
        showPaymentModal &&
        <Modal
          handleClose={handleClose}
          proceedPayment={proceedPayment}
          paymentModal={showPaymentModal}
        />
      }

      {error && (
        <LoginModal
          isOpen={error}
          openModal={handleOpenModal}
          closeModal={handleCloseModal}
          message={errorDetails}
          title={errorTitle}
        />
      )}

      {
        openQrCodeModal && <BridgeModalCom
          open={openQrCodeModal}
          handleClose={handleCloseQrCode}
          handleQrData={handleQrData}

        
        />
      }

      <Grid container spacing={2}>
        <Grid item lg={12}>
          <Appbar />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={8} lg={8}>
              <div className={classes.containerDiv}>
                <h1>Get Out</h1>

                <br />
                <Grid container spacing={2}>

                  <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <b style={{ fontWeight: "600" }}>TagId</b>

                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <br />

                      <TextField
                        className={classes.vehicleInput}
                        InputProps={{
                          classes: {
                            input: classes.resize
                          }
                        }}
                        size="small"
                        variant="outlined"
                        onChange={handleTag}
                        value={tagId}
                      />

                    </Grid>
                  </Grid>
                  <Grid item xs={12} sm={12} md={6} lg={6}> 
                    <Grid item xs={12} sm={12} md={12} lg={12}>

                      <b style={{ fontWeight: "600" }}>Tag Message</b>

                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <br />
                      <h3 >{tagMessage}</h3>


                    </Grid>
                  </Grid>
                  <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <b style={{ fontWeight: "600" }}>Vehicle No</b>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <br />
                      <TextField
                        className={classes.vehicleInput}
                        InputProps={{
                          classes: {
                            input: classes.resize
                          }

                        }}
                        size="small"
                        variant="outlined"
                        onChange={handleVRNumber}
                        value={VRNumber}
                      />
                      <br />
                    </Grid>
                  </Grid>

                  <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <b style={{ fontWeight: "600" }}>Number Plate Image</b>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <br />
                      <img style={{ height: "50px", width: "150px" }} src={anprImage && anprImage} alt="Img Tag" />
                    </Grid>
                  </Grid>


                  <Grid item xs={12} sm={12} md={6} lg={6}>

                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <label>
                        <b style={{ fontWeight: "600" }}>Booking Number</b>
                      </label>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <br />
                      <TextField
                        className={classes.vehicleInput}
                        InputProps={{
                          classes: {
                            input: classes.resize
                          }
                        }}
                        size="small"
                        variant="outlined"
                        value={bookingNo}
                        onChange={handleBookingNo}
                      // defaultValue="123452AK-20"
                      />
                    </Grid>
                  </Grid>
                  <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <b style={{ fontWeight: "600" }}>
                        QR Code 
                    </b>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <br />
                      <TextField
                        className={classes.vehicleInput}
                        InputProps={{
                          classes: {
                            input: classes.resize
                          }
                        }}
                        size="small"
                        variant="outlined"
                        // disabled={true}
                        type="password"
                        value={qrcode}
                        autoFocus={true}
                        onChange={(e)=>(handleQrData(e.target.value))}
                      />
                    </Grid>
                  </Grid>

                  <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <b style={{ fontWeight: "600" }}>
                        Vehicle Owner Number
                    </b>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <br />
                      <TextField
                        className={classes.vehicleInput}
                        InputProps={{
                          classes: {
                            input: classes.resize
                          }
                        }}
                        size="small"
                        variant="outlined"
                        disabled={true}
                        defaultValue={ownerName}

                      />
                    </Grid>
                  </Grid>
                  <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <b style={{ fontWeight: "600" }}>Vehicle Owner Name</b>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <br />
                      <TextField
                        className={classes.vehicleInput}
                        InputProps={{
                          classes: {
                            input: classes.resize
                          }

                        }}
                        size="small"
                        variant="outlined"
                        disabled={true}
                        defaultValue={ownerName}
                      />
                      <br />
                    </Grid>
                  </Grid>


                </Grid>
              </div>

              {/* <div className={classes.verticalLine}></div> */}
            </Grid>


            <Grid item xs={12} sm={12} md={4} lg={4}>

              {/* <div className={classes.gridFour}> */}
                <br />
                <h2 style={{ color: "#00BBDC" }}>Booking Details</h2>
                <Grid item xs={12} sm={12} md={12} lg={12}>

                  <div className={classes.root}>
                    
                    {charges &&
                      <List component="nav" aria-label="main mailbox folders">
                        <ListItem>
                          <ListItemText primary={"Booking No"} />
                          <ListItemText align="right" primary={charges && bookingNo} />
                        </ListItem>
                        <Divider />

                        <ListItem>

                          <ListItemText primary={"Charges"} />
                          <ListItemText align="right" primary={charges && charges.charge_type} />
                        </ListItem>
                        <Divider />

                        <ListItem>
                          <ListItemText primary="IN TIME" />
                          <ListItemText align="right" primary={charges && charges.entryTime} />
                        </ListItem>
                        <Divider />

                        <ListItem>
                          <ListItemText primary="OUT TIME" />
                          <ListItemText align="right" primary={charges && charges.exitTime} />
                        </ListItem>
                        <Divider />
                        <ListItem>
                          <ListItemText primary="Vehicle Type" />
                          <ListItemText align="right" primary={vehicleType && vehicleType} />
                        </ListItem>
                        <Divider />
                        <ListItem>
                          <ListItemText primary="User Type" />
                          <ListItemText align="right" primary={charges && charges.userType} />
                        </ListItem>
                        <Divider />

                        <ListItem>
                          <ListItemText primary="Sub Total:" />
                      RS.
                      <TextField
                            className={classes.subTotal}
                            align="right"
                            size="small"
                            variant="outlined"
                            value={value}
                            onChange={handleChange}
                          />
                        </ListItem>
                        <Divider />
                        <ListItem
                          style={{
                            color: "#00BBDC",
                            fontSize: "40px",
                            fontWeight: "bold"
                          }}
                        >
                          <ListItemText primary="Total Amount to Pay:" />
                          <ListItemText classes={{ primary: classes.listItemText }} align="right" primary={value} />
                        </ListItem>
                      </List>
                    }
                  </div>
                </Grid>

                {payNow ?
                  <Grid item xs={12} sm={12} md={12} lg={12}>

                    <div
                      onClick={() => (proceedPay("PAYNOW"))}

                      className={classes.printTicket} variant="contained">
                      <p className={classes.printTicketText}>Pay Now</p>
                    </div>
                  </Grid>
                  
                  :
                  <Grid item xs={12} sm={12} md={12} lg={12}>

                    <div
                      onClick={() => (proceedPay("GETOUT"))}
                      className={classes.printTicket} variant="contained">
                      <p className={classes.printTicketText}>CHECK OUT</p>
                    </div>
                  </Grid>
               



                }
              {/* </div> */}
            </Grid>
          </Grid>
        </Grid>
        <Tab  />
      </Grid>
    </div>
  );
}
