import React from "react";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Link, withRouter } from "react-router-dom";
import { useStyles } from "./css";

const isActive = (history, path) => {
  if (history.location.pathname === path)
    return {
      width: "100%",
      color: "#ffff ",
      backgroundColor: "#00BBCD",
      textAlign: "center",
      fontSize: "20px",
      padding: "10px",
      height: "40px",
      textDecoration: "none"
    };
  else
    return {
      width: "100%",
      color: "#00BBDC",
      backgroundColor: "#ffff",
      textAlign: "center",
      fontSize: "20px",
      padding: "10px",
      height: "40px",
      textDecoration: "none"
    };
};

const isActiveTab = (history, path) => {
  if (history.location.pathname === path)
    return {
      fontSize: "20px",
            fontWeight: "bold",
            textDecoration:'None',
            color:"white"
    };
  else
    return {
      fontSize: "20px",
      fontWeight: "bold",
      textDecoration:'None',
      
    };
};


const Tab1 = ({ history }) => {
  const classes = useStyles();
  return (
    <Tabs className={classes.mobileTab}>
      <Link
        to="/otopark/executive/getin"
        style={isActive(history, "/otopark/executive/getin")}
      >
        <Tab
          style={isActiveTab(history, "/otopark/executive/getin")}
          label="IN"
        />
      </Link>
      <Link
        to="/otopark/executive/getout"
        style={isActive(history, "/otopark/executive/getout")}
      >
        <Tab
          style={isActiveTab(history, "/otopark/executive/getout")}
          label="OUT"
        />
      </Link>
      <Link
        to="/otopark/executive/getmore"
        style={isActive(history, "/otopark/executive/getmore")}
      >
        <Tab
          style={isActiveTab(history, "/otopark/executive/getmore")}
          label="MORE"
        />
      </Link>
    </Tabs>
  );
};

export default withRouter(Tab1);
