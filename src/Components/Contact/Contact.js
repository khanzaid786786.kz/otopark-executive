import { Button } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import React from "react";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function AboutPage() {
  const classes = useStyles();

  return (
    <>
      <div className={classes.desktopView}>
        <Link to="/otopark/executive/getmore">
          <ArrowBackIcon style={{ color: "#00BBDC" }} />
        </Link>
      </div>
      <div className={classes.root}>
        <img
          width="150px"
          height="150px"
          src={require("../../assets/images/Rectangle335.png")}
        />
      </div>
      <div align="center">
        <h4 style={{ color: "#00BBDC" }}> Phone Number: 9111611134</h4>
        <h4 style={{ color: "#00BBDC" }}> E-mail: info@otopark.in</h4>
      </div>
    </>
  );
}
