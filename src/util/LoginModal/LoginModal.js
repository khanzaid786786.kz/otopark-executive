import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
// import classes from "*.module.sass";
import { useStyles } from "./css";

export default function ScrollDialog(props) {
  const classes = useStyles();
  const [scroll, setScroll] = React.useState("paper");
  const [maxWidth, setMaxWidth] = React.useState("md");

  //   const [open, setOpen] = React.useState(false);

  //   const handleClickOpen = scrollType => () => {
  //     setOpen(true);
  //     setScroll(scrollType);
  //   };

  //   const handleClose = () => {
  //     setOpen(false);
  //   };

  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (props.isOpen) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [props.isOpen]);

  return (
    <div>
      <Dialog
        // maxWidth={maxWidth}
        fullWidth={true}
        open={props.isOpen}
        onClose={props.closeModal}
        scroll={scroll}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle
          id="scroll-dialog-title"
          style={{ backgroundColor: "#00BBDC", color: "#fff" }}
        >
          {props.title ? props.title : "Warning !!"}
        </DialogTitle>

        <DialogContent
          dividers={scroll === "paper"}
          style={{ backgroundColor: "#f2f2f2" }}
        >
          <DialogContentText
            id="scroll-dialog-description"
            ref={descriptionElementRef}
            tabIndex={-1}
            className={classes.contentMainDiv}
          >
            <div className={classes.contentDiv}>
              <div>
                <img
                  style={{ width: "40px", height: "40px", padding: "6px" }}
                  src={require("../../assets/images/warning.png")}
                />
              </div>
              <div
                style={{ fontSize: "24px", fontWeight: "600", color: "black" }}
              >
                {props.message}
              </div>
            </div>
            <div style={{ display: "flex", justifyContent: "flex-end" }}>
              <Button
                onClick={props.closeModal}
                color="primary"
                className={classes.okButton}
              >
                Ok
              </Button>
            </div>
          </DialogContentText>
        </DialogContent>
        {/* <DialogActions style={{ backgroundColor: "red" }}>
          <Button onClick={props.closeModal} color="primary">
            Ok
          </Button>
        </DialogActions> */}
      </Dialog>
    </div>
  );
}
