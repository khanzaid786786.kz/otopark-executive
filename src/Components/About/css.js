import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    // marginTop:"10px"
    // backgroundColor:'#00BBDC',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "10px"
  },
  desktopView: {
    display: "none",
    [theme.breakpoints.down("sm")]: {
      display: "block"
    }
  }
}));
