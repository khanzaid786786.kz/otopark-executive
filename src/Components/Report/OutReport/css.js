import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    minWidth: 100,
    margin: "10px",
    width: "100%"
  },
  report: {
    overflow: "scroll",
    overflowX: "hidden",
    height: "500px",
    [theme.breakpoints.down("sm")]: {
      height: "100vh"
    }
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto"
    }
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit"
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "auto",
    [theme.breakpoints.up("md")]: {
      // width: "20ch"
      width: "auto"
    }
  },
  content: {
    fontSize: "15px"
  },
  viewMoreBtn: {
    textTransform: "none",
    width: "40%",
    color: "white",
    backgroundColor: "#00BBDC",
    borderRadius: "20px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    "&:hover": {
      color: "#00BBDC",
      backgroundColor: "white",
      border: "1px solid #00BBDC"
    }
  },
  viewTransactionBtn: {
    textTransform: "none",
    margin: "10px",
    padding: "10px",
    backgroundColor: "#00BBDC",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#00BBDC",
      border: "1px solid #00BBDC"
    }
  },
  desktopView: {
    display: "none",
    [theme.breakpoints.down("sm")]: {
      display: "block"
      // marginBottom: "20px"
    }
  }
}));
