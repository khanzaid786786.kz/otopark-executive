import { makeStyles, fade } from "@material-ui/core/styles";
import { borderColor } from "@material-ui/system";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: "10px",
    paddingBottom: "40px",
    [theme.breakpoints.down("sm")]: {
      padding: "10px"
    }
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  list: {
    padding: "40px",
    [theme.breakpoints.down("sm")]: {
      padding: "0px"
    }
  },
  profileImg: {
    marginLeft: "25px",
    height: "100px"
  },

  desktopView: {
    // display: "block",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  mobileView: {
    display: "none",
    marginTop: "25px",
    [theme.breakpoints.down("sm")]: {
      display: "block"
    }
  },
  logoutButton: {
    [theme.breakpoints.down("sm")]: {
      backgroundColor: "#00BBDC",
      color: "white"
    }
  }
}));
