import { makeStyles, fade } from "@material-ui/core/styles";
import { borderColor } from "@material-ui/system";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: "10px",
    "& label.Mui-focused": {
      color: "#00BBDC"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#00BBDC"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#00BBDC"
      },
      "&:hover fieldset": {
        borderColor: "#00BBDC"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#00BBDC"
      }
    }
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    backgroundColor: "#fff"
  },
  loginImage: {
    width: "150px",
    height: "100px",
    display: "block",
    marginTop: "20px",
    marginLeft: "auto",
    marginRight: "auto"
  },
  label: {
    color: "#00BBDC",
    fontWeight: "bold"
  },
  submitButton: {
    textTransform: "none",
    width: "80%",
    borderRadius: "20px",
    height: "50px",
    marginTop: "20px",
    backgroundColor: "#00BBDC",
    color: "white",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#00BBDC",
      border: "1px solid #00BBDC"
    },
    [theme.breakpoints.down("sm")]: {
      width: "40%"
    }
  },
  desktopView: {
    display: "none",
    [theme.breakpoints.down("sm")]: {
      display: "block"
    }
  }
}));
