import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    margin: "-8px",
    [theme.breakpoints.down("sm")]: {
      margin: "-8px"
    }
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  name: {
    fontSize: "20px",
    fontWeight: "bold",
    [theme.breakpoints.down("sm")]: {
      fontSize: "12px"
    }
  },
  nameId: {
    fontSize: "12px",
    fontWeight: "bold",
    marginTop: "-15px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "-8px",
      fontSize: "10px"
    }
  },
  title: {
    flexGrow: 1,
    marginLeft: "20px"
  },
  logoutButton: {
    color: "#00BBDC",
    textTransform: "none",
    [theme.breakpoints.down("sm")]: {
      display: "None"
    }
  },
  logoutIcon: {
    [theme.breakpoints.down("sm")]: {
      display: "None"
    }
  },
  mobileAppbar: {
    width: "100%",
    // display: "block",
    // position:'fixed',
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      position: "fixed"
    }
  }
}));
