import { Backdrop, Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, Grid, TextField, Typography } from '@material-ui/core'
import React, { useState,useEffect } from 'react'
import { useStyles } from "./css";
import Slide from '@material-ui/core/Slide';
import { tr } from 'date-fns/locale';
import { getTransactionDetails } from '../Service';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
export const Modal = (props) => {

    const classes = useStyles();
    return (
        <div style={{ padding: "20px", minWidth: '50%' }} className={classes.root}>

            <Dialog onClose={props.handleClose}
                fullWidth={true}
                aria-labelledby="customized-dialog-title" style={{ minWidth: '50%' }} open={props.paymentModal}>
                <DialogTitle id="customized-dialog-title" onClose={props.handleClose}>
                    Payment Mode
        </DialogTitle>
                <DialogContent dividers style={{ minWidth: '50%' }}>
                    <Typography gutterBottom style={{ minWidth: '50%' }}>
                        <Grid container spacing={3} style={{ minWidth: '50%' }}>
                            <Grid item xs={6} sm={6} onClick={() => (props.proceedPayment("CASH"))}>
                                <div className={classes.showPointer} style={{ borderRight: '2px solid lightgrey' }}>
                                    <img src={require('../assets/images/money.png')} style={{ width: "60px" }} />
                                </div>

                                <div className={classes.showPointer} style={{ color: "black" }}>
                                    CASH
                            </div>
                            </Grid>

                            <Grid item xs={6} sm={6} onClick={() => (props.proceedPayment("CARD"))}>
                                <div className={classes.showPointer}>
                                    <img src={require('../assets/images/credit-card.png')} style={{ width: "60px" }} />
                                </div>
                                <div className={classes.showPointer} style={{ color: "black" }}>
                                    CARD
                            </div>
                            </Grid>

                        </Grid>
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={props.handleClose} >
                        close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}


export const PrintModal = (props) => {
    const classes = useStyles();
    return (
        <div style={{ padding: "20px", minWidth: '50%' }} className={classes.root}>
            <Dialog 
                fullWidth={true}
                aria-labelledby="customized-dialog-title" style={{ minWidth: '50%' }} open={props.wantReceipt}>
                <DialogTitle id="customized-dialog-title" onClose={props.handleReceiptClose}>
                    Do You Want Receipt ?
        </DialogTitle>
                <DialogActions>
                    <Button primary onClick={props.printAndHandleClose}>
                        Yes
                    </Button>
                    <Button onClick={props.handleReceiptClose}>
                        No
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export const Challan = (props) => {
    const [chargeAmount, setChargeAmount] = useState(0);


    function handleCharge(e) {
        if (e.target.value.length == 0) {
            setChargeAmount(0);
        } else if (e.target.value < 0) {
            setChargeAmount(0);

        } else {
            setChargeAmount(e.target.value);
        }

    }


    function charge(val) {
        setChargeAmount(val);

    }

    const classes = useStyles();
    return (
        <div style={{ padding: "20px", minWidth: '50%' }} className={classes.root}>
            <Dialog onClose={props.handleCloseCallan}
                fullWidth={true}
                aria-labelledby="customized-dialog-title" style={{ minWidth: '50%' }} open={props.openChallan}>
                <DialogTitle id="customized-dialog-title" onClose={props.handleCloseCallan}>
                    Make Challan For {props.booking.VRNumber}
                </DialogTitle>
                <DialogContent dividers style={{ minWidth: '50%' }}>
                    <Typography gutterBottom style={{ minWidth: '50%' }}>
                        <Grid container spacing={3} style={{ minWidth: '50%' }}>
                            <Grid item xs={12} sm={12}>
                                <TextField onChange={handleCharge} value={chargeAmount} type="number" style={{ width: "100%" }} id="standard-basic" label="Amount" />

                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <Button color="primary" onClick={(e) => (charge("100"))}>&#8377;100</Button>
                                <Button color="secondary" onClick={(e) => (charge("200"))}>&#8377;200</Button>

                                <Button color="primary" onClick={(e) => (charge("500"))}>&#8377;500</Button>
                                <Button color="secondary" onClick={(e) => (charge("600"))}>&#8377;600</Button>
                                <Button color="primary" onClick={(e) => (charge("1000"))}>&#8377;1000</Button>
                                <Button color="secondary" onClick={(e) => (charge("499"))}>&#8377;499</Button>

                                <Button color="primary" onClick={(e) => (charge("999"))}>&#8377;999</Button>

                            </Grid>
                            {
                            /*<Grid  item xs={6} sm={6}  onClick={()=>(props.proceedPayment("CASH"))}>
                             <div className={classes.showPointer} style={{borderRight:'2px solid lightgrey'}}>
                                <img src={require('../assets/images/money.png')} style={{width:"60px"}}/>
                            </div>
                            
                            <div className={classes.showPointer} style={{color:"black"}}>
                                CASH
                            </div>
                        </Grid>
                        
                        <Grid item xs={6} sm={6}  onClick={()=>(props.proceedPayment("CARD"))}>
                            <div className={classes.showPointer}>
                                <img src={require('../assets/images/credit-card.png')} style={{width:"60px"}}/>
                            </div>
                            <div className={classes.showPointer} style={{color:"black"}}>
                                CARD
                            </div>
                        </Grid> */}

                        </Grid>
                    </Typography>
                </DialogContent>
                <DialogActions>

                    {
                        chargeAmount == 0 ? <Button onClick={props.handleCloseCallan}>Close</Button>
                            : <><Button onClick={props.handleCloseCallan}>Close</Button> <Button onClick={() => (props.proceedToPay(chargeAmount))}>
                                Proceed To Pay</Button></>
                    }

                </DialogActions>
            </Dialog>
        </div>
    )
}


// Report Modal
export const TransactionReport = (props) => {
    const [report,setReport]=useState(null);
    console.log(props);
    useEffect(()=>{
        let payload = sessionStorage.getItem("payload");
        if (!payload) {
        return;
         }
        payload = JSON.parse(payload);
        getTransactionDetails(payload.host, payload.parking_table_id, payload.ranger_id, props.booking.booking).then((res)=>{
           if(res.data.status==="success"){
            setReport(res.data.message);
           }
        }).catch((err)=>{
            console.log(err);
        })
    },[]);
    

    function printReceipt(){
        let payload = sessionStorage.getItem("payload");
        if (!payload) {
        return;
             }
       
        try {
            window.Android.printReceipt(report.VRNumber, props.booking.entryTime_new, props.booking.exitTime_new, payload.parking_address, payload.parking_name, report.vehicle_type, payload.host, payload.parking_table_id, payload.ranger_id, report.total_bill, report.user_type);
          } catch (error) {
            console.log(error)
          }
    }

    const classes = useStyles();
    return (
        <div style={{ padding: "20px", minWidth: '50%' }} className={classes.root}>
            <Dialog onClose={props.handleCloseTransactionReport}
                fullWidth={true}
                aria-labelledby="customized-dialog-title" style={{ minWidth: '50%' }} open={props.openTransactionReport}>
                <DialogTitle style={{backgroundColor:"#00BBDC", color:"#fff"}} id="customized-dialog-title" onClose={props.handleCloseTransactionReport}>
                 
                    <div style={{display:"flex",justifyContent:"space-between"}}>
                            Transaction Report Of {props.booking.VRNumber}
                            <Button className={classes.closeButton}  onClick={props.handleCloseTransactionReport} >
                                X
                            </Button>
                    </div>
                </DialogTitle>
                <DialogContent dividers>
                            <Grid item xs={12} sm={12} lg={12}>
                               <div className={classes.flexContainer}>
                                   

                                   <div className={classes.transactionText}>
                                       <p className={classes.txt}>BOOKING ID :</p>
                                    </div>
                                    <div className={classes.transactionVal}>
                                    <h2 className={classes.txt}>{report && report.booking_id}</h2>
                                    
                                    </div>
                                    
                                    <div className={classes.transactionText}>
                                       <p className={classes.txt}>USER TYPE :</p>
                                    </div>
                                    <div className={classes.transactionVal}>
                                    <h2 className={classes.txt}>{report && report.user_type}</h2>
                                    
                                    </div>

                                    
                                   <div className={classes.transactionText}>
                                       <p className={classes.txt}>VEHICLE TYPE :</p>
                                    </div>
                                    <div className={classes.transactionVal}>
                                    <h2 className={classes.txt}>{report && report.vehicle_type}</h2>
                                    
                                    </div>

                                    
                                   <div className={classes.transactionText}>
                                       <p className={classes.txt}>VEHICLE NO :</p>
                                    </div>
                                    <div className={classes.transactionVal}>
                                    <h2 className={classes.txt}>{report && report.VRNumber}</h2>
                                    
                                    </div>
                                    {
                                        report && report.user_type==="PASS" &&
                                        <div className={classes.transactionText}>
                                       <p className={classes.txt}>PASS START DATE :</p>
                                    </div>
                                    
                                    }
                                    {
                                        report && report.user_type==="PASS" &&
                                        <div className={classes.transactionVal}>
                                        <h2 className={classes.txt}>{report.pass_start_date}</h2>
                                        
                                        </div>
                                        }
                                    
                                    {
                                        report && report.user_type==="PASS" &&
                                        <div className={classes.transactionText}>
                                       <p className={classes.txt}>PASS END DATE :</p>
                                    </div>
                                    
                                    }
                                    {
                                        report && report.user_type==="PASS" &&
                                    
                                        <div className={classes.transactionVal}>
                                        <h2 className={classes.txt}>{report.pass_end_date}</h2>
                                        
                                        </div>
    
    
                                    }
                                    {
                                        report && report.user_type!=="PASS" &&
                                    
                                        <div className={classes.transactionText}>
                                       <p className={classes.txt}>ENTRY TIME :</p>
                                    </div>
                                   
                                    }
                                    
                                    {
                                        report && report.user_type!=="PASS" &&
                                    
                                        <div className={classes.transactionVal}>
                                        <h2 className={classes.txt}>{props.booking.entryTime_new}</h2>
                                        
                                        </div>
    
    
                                    }
                                    {
                                        report && report.user_type!=="PASS" &&
                                    
                                        <div className={classes.transactionText}>
                                        <p className={classes.txt}>EXIT TIME :</p>
                                     </div>
                                    
                                    }
                                    
                                    {
                                        report && report.user_type!=="PASS" &&
                                    
                                        <div className={classes.transactionVal}>
                                        <h2 className={classes.txt}>{props.booking.exitTime_new}</h2>
                                        
                                        </div>
    
    
                                    }
                                    
                                    

                                   
                                   {
                                       report && report.user_type!=="PASS" &&
                                       <div className={classes.transactionText}>
                                       <p className={classes.txt}>ADVANCE :</p>
                                    </div>    
                                   }
                                   
                                   {
                                       report && report.user_type!=="PASS" &&
                                       <div className={classes.transactionVal}>
                                    <h2 className={classes.txt}>{report.advance}</h2>
                                    
                                    </div>
 
                                   }
                                   
                                   {
                                       report && report.user_type==="PASS" &&
                                       <div className={classes.transactionText}>
                                       <p className={classes.txt}>DISCOUNT :</p>
                                    </div>    
                                   }
                                   
                                   {
                                       report && report.user_type==="PASS" &&
                                       <div className={classes.transactionVal}>
                                    <h2 className={classes.txt}>{report.discount}</h2>
                                    
                                    </div>
 
                                   }
                                   
                                    
                                   <div className={classes.transactionText}>
                                       <p className={classes.txt}>TOTAL BILL :</p>
                                    </div>
                                    <div className={classes.transactionVal}>
                                    <h2 className={classes.txt}>{report && report.total_bill}</h2>
                                    
                                    </div>


                                   {
                                       report && report.user_type==="PASS" &&
                                    <div className={classes.transactionText}>
                                       <p className={classes.txt}>ACTUAL BILL :</p>
                                    </div>

                                    }
                                    {
                                       report && report.user_type==="PASS" &&
                                   
                                    <div className={classes.transactionVal}>
                                    <h2 className={classes.txt}>{report && report.actual_bill}</h2>
                                    
                                    </div>
                                    }
                                    
                                   
                                    <div>

                                    
                                    <Button className={classes.printButton} onClick={printReceipt} variant="contained" color="primary">
                                            Print
                                    </Button>
                                    </div>
                                                                       
                                </div>
                            
                     
                        </Grid>
                </DialogContent>
                {/* <DialogActions>
                  
                </DialogActions> */}
            </Dialog>
        </div>
    )
}

export const ChallanPaymentTypeModal = (props) => {



    const classes = useStyles();
    return (
        <div style={{ padding: "20px", minWidth: '50%' }} className={classes.root}>

            <Dialog onClose={props.handleClose}
                fullWidth={true}
                aria-labelledby="customized-dialog-title" style={{ minWidth: '50%' }} open={props.paymentModal}>
                <DialogTitle id="customized-dialog-title" onClose={props.handleClose}>
                    Payment Mode
        </DialogTitle>
                <DialogContent dividers style={{ minWidth: '50%' }}>
                    <Typography gutterBottom style={{ minWidth: '50%' }}>
                        <Grid container spacing={3} style={{ minWidth: '50%' }}>
                            <Grid item xs={4} sm={4} onClick={() => (props.proceedPayment("CASH"))}>
                                <div className={classes.showPointer} style={{ borderRight: '2px solid lightgrey' }}>
                                    <img src={require('../assets/images/money.png')} style={{ width: "60px" }} />
                                </div>

                                <div className={classes.showPointer} style={{ color: "black" }}>
                                    CASH
                            </div>
                            </Grid>

                            <Grid item xs={4} sm={4} onClick={() => (props.proceedPayment("CARD"))}>
                                <div className={classes.showPointer} style={{ borderRight: '2px solid lightgrey' }}>
                                    <img src={require('../assets/images/credit-card.png')} style={{ width: "60px" }} />
                                </div>
                                <div className={classes.showPointer} style={{ color: "black" }}>
                                    CARD
                            </div>
                            </Grid>

                            <Grid item xs={4} sm={4} onClick={() => (props.proceedPayment("PAYONEXIT"))}>
                                <div className={classes.showPointer}>
                                    <img src={require('../assets/images/receipt-solid.svg')} style={{ width: "60px" }} />
                                </div>
                                <div className={classes.showPointer} style={{ color: "black" }}>
                                    Pay On Exit
                            </div>
                            </Grid>

                        </Grid>
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={props.handleClose} >
                        close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export const BridgeModalCom = function BridgeModal(props){
    const classes = useStyles();

    return(
    <div style={{ padding: "20px", minWidth: '50%' }} className={classes.root}>

        <Backdrop className={classes.backdrop} open={props.open} onClick={props.handleClose}>
            <CircularProgress color="inherit" />
            <Bridge ref={component => window.userComponentInstance = component} handleClose = {props.handleClose} callback={props.handleQrData}/>
        </Backdrop>
        </div>

);
}

export const BridgeCardModalCom = function BridgeModal(props){
    const classes = useStyles();

    return(
    <div style={{ padding: "20px", minWidth: '50%' }} className={classes.root}>

        <Backdrop className={classes.backdrop} open={props.open} onClick={props.handleClose}>
            <CircularProgress color="inherit" />
            <BridgeCardPayment ref={component => window.userComponentInstance = component} handleClose = {props.handleClose} callback={props.handleCardData} vehicleNo={props.vehicleNo} amount={props.amount}/>
        </Backdrop>
        </div>

);
}


class Bridge extends React.Component{
    


    closeModal=(val)=>{
        this.props.handleQrData(val);
        this.props.handleClose();

    }
    componentDidMount=()=>{
        try {
            window.Android.scanQR();
          }
          catch(err) {
            // document.getElementById("demo").innerHTML = err.message;
            console.log(err);  
        
        }
        }
    render(){
        return(<></>);
    }
}

class BridgeCardPayment extends React.Component{
    


    closeModal=(val)=>{
        this.props.handleCardPaymentData(val);
        this.props.handleClose();

    }
    componentDidMount=()=>{
        try {
            window.Android.cardPayment(this.props.amount,this.props.vehicleNo);
          }
          catch(err) {
            // document.getElementById("demo").innerHTML = err.message;
        console.log(err);  
        }
        }
    render(){
        return(<></>);
    }
}
