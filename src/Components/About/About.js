import { Button } from "@material-ui/core";
import React from "react";
import { useStyles } from "./css";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Link } from "react-router-dom";

export default function AboutPage() {
  const classes = useStyles();

  return (
    <>
      <div className={classes.desktopView}>
      <Link to="/otopark/executive/getmore">
        <ArrowBackIcon style={{ color: "#00BBDC" }} />
        </Link>
      </div>
      <div className={classes.root}>
        <img
          src={require("../../assets/images/Rectangle335.png")}
          style={{ height: "120px" }}
        />
      </div>

      <h3 style={{ color: "#00BBDC", fontWeight: "600" }}>About Us</h3>
      <div>
        otopark is a dynamic organisation that exists to meet the parking
        challenges of the mega cities of India, by providing smart sustainable
        innovative technological solutions. otopark focuses on building a
        collaborative economy between consumers, businesses and government
        entities for efficient allocation of community resources.
      </div>

      <div className={classes.root}>For any queries call otopark helpline:</div>
      <div className={classes.root}>
        <Button
          variant="contained"
          style={{
            width: "80%",
            color: "white",
            backgroundColor: "#00BBDC",
            borderRadius: "20px"
          }}
          size="large"
        >
          Call Us
        </Button>
      </div>
      <div className={classes.root}>
        2019 © All Rights Reserved.
        <br />
        otopark Technological Services Pvt. Ltd.
      </div>
    </>
  );
}
