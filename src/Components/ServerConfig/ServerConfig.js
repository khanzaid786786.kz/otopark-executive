import {
  Button,
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField
} from "@material-ui/core";
import React from "react";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export const ServerConfig = props => {
  const classes = useStyles();
  const [value, setValue] = React.useState("local");
  const handleChange = event => {
    setValue(event.target.value);
  };
  return (
    <div className={classes.root}>
      <Grid container spacing={2} >
        <Grid style={{backgroundColor: "#00BBDC" }} item xs={12} sm={12} md={12} lg={12}>
          <div >
            <Grid
              spacing={2}
              container
              direction="row"
              justify="left"
              alignItems="center"
              
            >
              <Link to="/otopark/executive/getmore">
                <ArrowBackIcon
                  className={classes.desktopView}
                  style={{ color: "white" }}
                />
              </Link>
              <i
                class="fa fa-cog"
                aria-hidden="true"
                style={{ fontSize: "25px", color: "#ffff", padding: "10px" }}
              ></i>
              <h3 style={{ color: "#ffff" }}>Server Configuration</h3>
            </Grid>
          </div>
          </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>        
          <FormControl component="fieldset" style={{ padding: "10px" }}>
            <RadioGroup
              aria-label="gender"
              name="server"
              value={value}
              onChange={handleChange}
            >
              <div>
                <FormControlLabel
                  value="local"
                  control={<Radio style={{ color: "#00BBDC" }} />}
                />

                <TextField
                  id="outlined-basic"
                  variant="outlined"
                  label="local"
                  className={classes.textField}
                  
                />
              </div>
              <br />
              {/* <TextField id="outlined-basic" control={<Radio />} variant="outlined" /> */}
              <div>
                <FormControlLabel
                  value="live"
                  control={<Radio style={{ color: "#00BBDC" }} />}
                />

                <TextField
                  id="outlined-basic"
                  variant="outlined"
                  label="live"
                  className={classes.textField}
                  
                />
              </div>
            </RadioGroup>
           
          </FormControl>
          <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                padding: "20px"
              }}
            >
              <Button
                style={{
                  color: "white",
                  backgroundColor: "#00BBDC",
                  width: "100%"
                }}
              >
                SAVE
              </Button>
            </div>
        </Grid>
      </Grid>
    </div>
  );
};
export default ServerConfig;
